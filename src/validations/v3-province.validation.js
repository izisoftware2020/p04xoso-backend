const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const getProvinces = {
  params: Joi.object().keys({
    region: Joi.string().custom(objectId),
    symbol: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

module.exports = {
  getProvinces,
};
