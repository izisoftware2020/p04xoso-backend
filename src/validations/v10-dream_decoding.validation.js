const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const findDreamDecodingsByContent = {
  body: Joi.object().keys({
    content: Joi.string().required(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

module.exports = {
  findDreamDecodingsByContent,
};
