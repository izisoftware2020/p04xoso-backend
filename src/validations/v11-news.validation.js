const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const findById = {
  params: Joi.object().keys({
    id: Joi.string().required().custom(objectId),
  }),
};

module.exports = {
  findById,
};
