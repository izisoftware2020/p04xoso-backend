const Joi = require('joi');
const { objectId } = require('./custom.validation');

const create = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    body: Joi.string().required(),
    mainImage: Joi.string().required(),
    tags: Joi.string().required(),
    dateCreated: Joi.string(),
  }),
};

const findByIdAndUpdate = {
  body: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
    title: Joi.string().required(),
    body: Joi.string().required(),
    mainImage: Joi.string().required(),
    tags: Joi.string().required(),
    dateCreated: Joi.string(),
  }),
};

const findByIdAndDelete = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

const paginate = {
  query: Joi.object().keys({
    page: Joi.string().optional(),
    limit: Joi.number().max(100).optional(),
  }),
};

const findById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
  }),
};

module.exports = {
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  paginate,
  findById,
};
