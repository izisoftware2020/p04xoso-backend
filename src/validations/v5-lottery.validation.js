const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const getLotterys = {
  query: Joi.object().keys({
    region: Joi.string().custom(objectId),
    province: Joi.string().custom(objectId),
    dateStart: Joi.number().integer(),
    dateEnd: Joi.number().integer(),
    timestamp: Joi.number().integer().optional(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const checkLotteryTickets = {
  query: Joi.object().keys({
    idProvince: Joi.string().required().custom(objectId),
    lottery: Joi.string(),
    dateStart: Joi.number().integer(),
  }),
};

const statisThePairsOfNumberThatAppearMost = {
  query: Joi.object().keys({
    idRegion: Joi.string().custom(objectId),
    idProvince: Joi.string().custom(objectId),
    dateStart: Joi.number().integer(),
    dateEnd: Joi.number().integer(),
  }),
};

const statisAppearOfNumbersWinningFromResult = {
  query: Joi.object().keys({
    lottery: Joi.string().required(),
    idProvince: Joi.string().required().custom(objectId),
    dateStart: Joi.number().integer(),
    dateEnd: Joi.number().integer(),
  }),
};

const statisLotoGan = {
  query: Joi.object().keys({
    type: Joi.number().integer().required(),
    lottery: Joi.string().required(),
    idProvince: Joi.string().required().custom(objectId),
    dateStart: Joi.number().integer().required(),
    dateEnd: Joi.number().integer().required(),
  }),
};

const statisLotoWithTheLastTwoDigits = {
  query: Joi.object().keys({
    idProvince: Joi.string().required().custom(objectId),
    dateStart: Joi.number().integer().required(),
    dateEnd: Joi.number().integer().required(),
  }),
};

const statisBasicArrayNumberFrom01To99 = {
  query: Joi.object().keys({
    idProvince: Joi.string().required().custom(objectId),
    type: Joi.string().required(),
    dateStart: Joi.number().integer().required(),
    dateEnd: Joi.number().integer().required(),
  }),
};

const statisCauBachThu = {
  query: Joi.object().keys({
    idProvince: Joi.string().required().custom(objectId),
    amplitude: Joi.string().required(),
    dateStart: Joi.number().integer().required(),
    dateEnd: Joi.number().integer().required(),
  }),
};

const statisHeadAndTailFrom0To9 = {
  query: Joi.object().keys({
    type: Joi.string().required(),
    idProvince: Joi.string().required().custom(objectId),
    dateStart: Joi.number().integer().required(),
    dateEnd: Joi.number().integer().required(),
  }),
};

module.exports = {
  getLotterys,
  checkLotteryTickets,
  statisThePairsOfNumberThatAppearMost,
  statisAppearOfNumbersWinningFromResult,
  statisLotoGan,
  statisLotoWithTheLastTwoDigits,
  statisBasicArrayNumberFrom01To99,
  statisCauBachThu,
  statisHeadAndTailFrom0To9,
};
