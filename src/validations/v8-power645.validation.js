const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const getPower645s = {
  dateStart: Joi.number().integer(),
  sortBy: Joi.string(),
  limit: Joi.number().integer(),
  page: Joi.number().integer(),
  month: Joi.number().integer(),
  year: Joi.number().integer(),
};

const findThePairsAppearTheMost = {
  query: Joi.object().keys({
    dateStart: Joi.string().required(),
    dateEnd: Joi.string().required(),
  }),
};

const countTheFrequencyOfOccurrenceOfPairs = {
  query: Joi.object().keys({
    dateStart: Joi.string().required(),
    dateEnd: Joi.string().required(),
  }),
};

const checkPower645Tickets = {
  query: Joi.object().keys({
    lottery: Joi.string().required(),
    dateStart: Joi.number().required(),
  }),
};

module.exports = {
  getPower645s,
  countTheFrequencyOfOccurrenceOfPairs,
  checkPower645Tickets,
  findThePairsAppearTheMost,
};
