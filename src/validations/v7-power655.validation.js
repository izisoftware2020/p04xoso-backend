const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const getPower655s = {
  dateStart: Joi.number().integer(),
  sortBy: Joi.string(),
  limit: Joi.number().integer(),
  page: Joi.number().integer(),
  month: Joi.number().integer(),
  year: Joi.number().integer(),
};

const findThePairsAppearTheMost = {
  query: Joi.object().keys({
    dateStart: Joi.number().required(),
    dateEnd: Joi.number().required(),
  }),
};

const countTheFrequencyOfOccurrenceOfPairs = {
  query: Joi.object().keys({
    dateStart: Joi.number().required(),
    dateEnd: Joi.number().required(),
  }),
};

const checkPower655Tickets = {
  query: Joi.object().keys({
    lottery: Joi.string().required(),
    dateStart: Joi.number().required(),
  }),
};

module.exports = {
  getPower655s,
  findThePairsAppearTheMost,
  countTheFrequencyOfOccurrenceOfPairs,
  checkPower655Tickets,
};
