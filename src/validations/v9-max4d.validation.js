const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const getMax4Ds = {
  dateStart: Joi.number().integer(),
  sortBy: Joi.string(),
  limit: Joi.number().integer(),
  page: Joi.number().integer(),
  month: Joi.number().integer(),
  year: Joi.number().integer(),
};

const checkMax4dTickets = {
  query: Joi.object().keys({
    lottery: Joi.string().required(),
    dateStart: Joi.number().required(),
  }),
};

module.exports = {
  getMax4Ds,
  checkMax4dTickets,
};
