module.exports.authValidation = require('./auth.validation');
module.exports.userValidation = require('./user.validation');

// Admin
module.exports.menuValidation = require('./20menu.validation');
module.exports.roleValidation = require('./30role.validation');
module.exports.roleDetailValidation = require('./40role-detail.validation');
module.exports.bannerValidation = require('./50banner.validation');
module.exports.newsValidation = require('./60news.validation');

module.exports.soiCauValidation = require('./70soi-cau.validation');
module.exports.loaiValidation = require('./80loai.validation');
