const Joi = require('joi');
const { objectId } = require('./custom.validation');

const create = {
  body: Joi.object().keys({
    menu: Joi.string().custom(objectId).optional(),
    isGroup: Joi.string().required(),
    name: Joi.string().required(),
    slug: Joi.string().required(),
    icon: Joi.string().required(),
    position: Joi.string().required(),
  }),
};

const findByIdAndUpdate = {
  body: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
    menu: Joi.string().custom(objectId).required(),
    isGroup: Joi.string().required(),
    name: Joi.string().required(),
    slug: Joi.string().required(),
    icon: Joi.string().required(),
    position: Joi.string().required(),
  }),
};

const findByIdAndDelete = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

const paginate = {
  query: Joi.object().keys({
    page: Joi.string().optional(),
    limit: Joi.number().max(100).optional(),
    menu: Joi.string().optional(),
  }),
};

const findById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId).required(),
  }),
};

module.exports = {
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  paginate,
  findById,
};
