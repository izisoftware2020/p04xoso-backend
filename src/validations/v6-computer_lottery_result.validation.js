const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const getComputerLotteryResults = {
  query: Joi.object().keys({
    dateStart: Joi.number().integer(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

module.exports = {
  getComputerLotteryResults,
};
