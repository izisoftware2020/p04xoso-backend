const Constant = {
  ID_NORTH_SIDE: '61139f4404eac908ce087370',
  ID_CENTRAL_SIDE: '61139f4a04eac908ce087374',
  ID_SOUTH_SIDE: '611547c017b6f56bf8584275',

  ID_PROVINCE_NORTH_SIDE: '611609c296a5db67d851bfd3',
  ID_REGION_NORTH_SIDE: '611609c296a5db67d851bfd3',
  ID_REGION_CENTRAL_SIDE: '611609c296a5db67d851bfd4',
  ID_REGION_SOUTH_SIDE: '611609c296a5db67d851bfd5',

  // URL link live of three side
  LINK_URL_LIVE_NORTH_SIDE: 'https://www.minhngoc.net.vn/xo-so-truc-tiep/mien-bac.html',
  LINK_URL_LIVE_CENTRAL_SIDE: 'https://www.minhngoc.net.vn/xo-so-truc-tiep/mien-trung.html',
  LINK_URL_LIVE_SOUTH_SIDE: 'https://www.minhngoc.net.vn/xo-so-truc-tiep/mien-nam.html',

  // URL link live of power 656
  LINK_URL_LIVE_POWER645: 'https://www.minhchinh.com/truc-tiep-xo-so-tu-chon-mega-645.html',

  // URL link live of power 655
  LINK_URL_LIVE_POWER655: 'https://www.minhchinh.com/truc-tiep-xo-so-tu-chon-power-655.html',

  // URL link live of mega
  LINK_URL_LIVE_MAX4d: 'https://www.minhchinh.com/truc-tiep-xo-so-tu-chon-max-4d.html',
};
module.exports = Constant;
