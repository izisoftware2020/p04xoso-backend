const express = require('express');
const validate = require('../../middlewares/validate');
const provinceController = require('../../controllers/c3-province.controller');
const provinceValidation = require('../../validations/v3-province.validation');

const router = express.Router();

router
  .route('/')
  .post(provinceController.createProvince)
  .get(validate(provinceValidation.getProvinces), provinceController.getProvinces);

module.exports = router;
