const express = require('express');
const { roleControler } = require('../../controllers');
const { roleValidation } = require('../../validations');
const auth = require('../../middlewares/auth');

const router = express.Router();
const validate = require('../../middlewares/validate');

router
  .route('/')
  .get(auth(), roleControler.find)
  .post(auth(), validate(roleValidation.create), roleControler.create)
  .put(auth(), validate(roleValidation.findByIdAndUpdate), roleControler.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(roleControler.paginate), roleControler.paginate);

router
  .route('/:id')
  .get(auth(), validate(roleValidation.findById), roleControler.findById)
  .delete(auth(), validate(roleValidation.findByIdAndDelete), roleControler.findByIdAndDelete);

module.exports = router;
