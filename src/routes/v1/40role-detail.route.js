const express = require('express');
const { roleDetailControler } = require('../../controllers');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const { roleDetailValidation } = require('../../validations');

const router = express.Router();

router
  .route('/')
  .get(auth(), roleDetailControler.find)
  .post(auth(), validate(roleDetailValidation.create), roleDetailControler.create)
  .put(auth(), validate(roleDetailValidation.findByIdAndUpdate), roleDetailControler.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(roleDetailValidation.paginate), roleDetailControler.paginate);

router
  .route('/:id')
  .get(auth(), validate(roleDetailValidation.findById), roleDetailControler.findById)
  .delete(auth(), validate(roleDetailValidation.findByIdAndDelete), roleDetailControler.findByIdAndDelete);

module.exports = router;
