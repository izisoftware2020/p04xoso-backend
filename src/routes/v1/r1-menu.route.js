const express = require('express');
const menuController = require('../../controllers/c1-menu.controller');

const router = express.Router();

router.route('/').post(menuController.createMenu).get(menuController.getMenus);

module.exports = router;
