const express = require('express');
const menu2Controller = require('../../controllers/c0-menu2.controller');

const router = express.Router();

router.route('/').post(menu2Controller.createMenu2).get(menu2Controller.getMenu2s);

module.exports = router;
