const express = require('express');

const router = express.Router();
const validate = require('../../middlewares/validate');
const lotteryController = require('../../controllers/c5-lottery.controller');
const lotteryValidation = require('../../validations/v5-lottery.validation');

router
  .route('/')
  .post(lotteryController.createLottery)
  .get(validate(lotteryValidation.getLotterys), lotteryController.getLotterys);

// Dò xổ số
router.route('/check').get(validate(lotteryValidation.checkLotteryTickets), lotteryController.checkLotteryTickets);

// Thống kê cơ bản
router
  .route('/statis/basic')
  .get(
    validate(lotteryValidation.statisThePairsOfNumberThatAppearMost),
    lotteryController.statisThePairsOfNumberThatAppearMost
  );

router
  .route('/statis/basic/01-99')
  .get(validate(lotteryValidation.statisBasicArrayNumberFrom01To99), lotteryController.statisBasicArrayNumberFrom01To99);

// Thống kê đầu và đuôi
router
  .route('/statis/basic/head-tail')
  .get(validate(lotteryValidation.statisHeadAndTailFrom0To9), lotteryController.statisHeadAndTailFrom0To9);

// Thống kê loto
router
  .route('/statis/loto')
  .get(
    validate(lotteryValidation.statisAppearOfNumbersWinningFromResult),
    lotteryController.statisAppearOfNumbersWinningFromResult
  );

// Thống kê gan
router.route('/statis/loto/gan').get(validate(lotteryValidation.statisLotoGan), lotteryController.statisLotoGan);

// Thống kê loto theo tổng
router
  .route('/statis/loto/total')
  .get(validate(lotteryValidation.statisLotoWithTheLastTwoDigits), lotteryController.statisLotoWithTheLastTwoDigits);

// Thống kê loto bạch thủ
router.route('/statis/loto/bachthu').get(validate(lotteryValidation.statisCauBachThu), lotteryController.statisCauBachThu);

module.exports = router;
