const express = require('express');
const regionController = require('../../controllers/c2-region.controller');
const auth = require('../../middlewares/auth');

const router = express.Router();

router.route('/').post(regionController.createRegion).get(regionController.getRegions);

router
  .route('/:id')
  .get(regionController.findByIdRegion)
  .delete(regionController.deleteByIdRegion)
  .post(regionController.updateByIdRegion);

module.exports = router;
