const express = require('express');
const { loaiController } = require('../../controllers');
const validate = require('../../middlewares/validate');
const { loaiValidation } = require('../../validations');
const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/')
  .get(auth(), loaiController.find)
  .post(auth(), validate(loaiValidation.create), loaiController.create)
  .put(auth(), validate(loaiValidation.findByIdAndUpdate), loaiController.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(loaiValidation.paginate), loaiController.paginate);

router
  .route('/:id')
  .get(auth(), validate(loaiValidation.findById), loaiController.findById)
  .delete(auth(), validate(loaiValidation.findByIdAndDelete), loaiController.findByIdAndDelete);

module.exports = router;
