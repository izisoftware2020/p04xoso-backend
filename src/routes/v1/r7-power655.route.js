const express = require('express');
const power655Controller = require('../../controllers/c7-power655.controller');
const power655Validate = require('../../validations/v7-power655.validation');
const validate = require('../../middlewares/validate');

const router = express.Router();

router
  .route('/')
  .post(power655Controller.createPower655)
  .get(validate(power655Validate.getPower655s), power655Controller.getPower655s);

router
  .route('/static-the-pairs-appear-the-most')
  .get(validate(power655Validate.findThePairsAppearTheMost), power655Controller.findThePairsAppearTheMost);

router
  .route('/static-frequency-of-pairs')
  .get(
    validate(power655Validate.countTheFrequencyOfOccurrenceOfPairs),
    power655Controller.countTheFrequencyOfOccurrenceOfPairs
  );

// Dò kết quả
router.route('/check').get(validate(power655Validate.checkPower655Tickets), power655Controller.checkPower655Tickets);

module.exports = router;
