const express = require('express');
const authRoute = require('./auth.route');
const docsRoute = require('./docs.route');
const config = require('../../config/config');
const menu1Route = require('./r0-menu1.route');
const menu2Route = require('./r0-menu2.route');
const userRoute = require('./r0-user.route');

const regionRoute = require('./r2-region.route');
const provinceRoute = require('./r3-province.route');
const prizeOpenScheduleRoute = require('./r4-prize_open_schedule.route');
const lotteryRoute = require('./r5-lottery.route');
const computerLotteryResultRoute = require('./r6-computer_lottery_result.route');
const power655Route = require('./r7-power655.route');
const power645Route = require('./r8-power645.route');
const max4DRoute = require('./r9-max4d.route');
const dreamDecodingRoute = require('./r10-dream_decoding.route');
const scheduler = require('./r12-scheduler.route');

// Admin
const menuRoute = require('./20menu.route');
const roleRoute = require('./30role.route');
const roleDetailRoute = require('./40role-detail.route');
const bannerRoute = require('./50banner.route');
const newsRoute = require('./60news.route');
const uploadRoute = require('./999999-upload.route');

const soiCauRoute = require('./70soi-cau.route');
const loaiRoute = require('./80loai.route');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/menus',
    route: menuRoute,
  },
  {
    path: '/menu1s',
    route: menu1Route,
  },
  {
    path: '/menu2s',
    route: menu2Route,
  },
  {
    path: '/regions',
    route: regionRoute,
  },
  {
    path: '/provinces',
    route: provinceRoute,
  },
  {
    path: '/prize-open-schedules',
    route: prizeOpenScheduleRoute,
  },
  {
    path: '/lotterys',
    route: lotteryRoute,
  },
  {
    path: '/computer-lottery-results',
    route: computerLotteryResultRoute,
  },
  {
    path: '/power655s',
    route: power655Route,
  },
  {
    path: '/dream-decodings',
    route: dreamDecodingRoute,
  },
  {
    path: '/max4ds',
    route: max4DRoute,
  },
  {
    path: '/news',
    route: newsRoute,
  },
  {
    path: '/power645s',
    route: power645Route,
  },
  {
    path: '/schedulers',
    route: scheduler,
  },
  {
    path: '/menus',
    route: menuRoute,
  },
  {
    path: '/roles',
    route: roleRoute,
  },
  {
    path: '/role-details',
    route: roleDetailRoute,
  },
  { path: '/banners', route: bannerRoute },
  { path: '/newss', route: newsRoute },
  { path: '/uploads', route: uploadRoute },
  { path: '/soi-caus', route: soiCauRoute },
  { path: '/loais', route: loaiRoute },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
