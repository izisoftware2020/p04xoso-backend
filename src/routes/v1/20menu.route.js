const express = require('express');
const { menuControler } = require('../../controllers');
const validate = require('../../middlewares/validate');
const auth = require('../../middlewares/auth');
const { menuValidation } = require('../../validations');

const router = express.Router();

router
  .route('/')
  .get(auth(), menuControler.find)
  .post(auth(), menuControler.create)
  .put(auth(), menuControler.findByIdAndUpdate)
  .delete(auth(), menuControler.findByIdAndDelete);

router.route('/paginate').get(auth(), validate(menuValidation.paginate), menuControler.paginate);

router
  .route('/:id')
  .get(auth(), validate(menuValidation.findById), menuControler.findById)
  .delete(auth(), validate(menuValidation.findByIdAndDelete), menuControler.findByIdAndDelete);

module.exports = router;
