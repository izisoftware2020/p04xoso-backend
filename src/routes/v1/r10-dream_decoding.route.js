const express = require('express');
const dreamDecodingController = require('../../controllers/c10-dream_decoding.controller');
const auth = require('../../middlewares/auth');
const dreamDecodingValidation = require('../../validations/v10-dream_decoding.validation');
const validate = require('../../middlewares/validate');

const router = express.Router();

router.route('/').post(dreamDecodingController.createDreamDecoding).get(dreamDecodingController.getDreamDecodings);

router
  .route('/decode')
  .post(validate(dreamDecodingValidation.findDreamDecodingsByContent), dreamDecodingController.findDreamDecodingsByContent);

module.exports = router;
