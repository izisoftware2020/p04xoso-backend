const express = require('express');
const { bannerControler } = require('../../controllers');
const validate = require('../../middlewares/validate');
const { bannerValidation } = require('../../validations');
const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/')
  .get(bannerControler.find)
  .post(auth(), validate(bannerValidation.create), bannerControler.create)
  .put(auth(), validate(bannerValidation.findByIdAndUpdate), bannerControler.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(bannerValidation.paginate), bannerControler.paginate);

router
  .route('/:id')
  .get(auth(), validate(bannerValidation.findById), bannerControler.findById)
  .delete(auth(), validate(bannerValidation.findByIdAndDelete), bannerControler.findByIdAndDelete);

module.exports = router;
