const express = require('express');
const menu1Controller = require('../../controllers/c0-menu1.controller');

const router = express.Router();

router.route('/').post(menu1Controller.createMenu1).get(menu1Controller.getMenu1s);

module.exports = router;
