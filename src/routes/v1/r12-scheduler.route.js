const express = require('express');
const schedulerController = require('../../controllers/c12-scheduler.controller');
const validate = require('../../middlewares/validate');
const newsValidation = require('../../validations/v11-news.validation');

const router = express.Router();

// router
//     .route('/schedulejob')
//     .get(schedulerController.schedulejob);

// router
//     .route('/startJob')
//     .get(schedulerController.startJob);

// router
//     .route('/stopJob')
//     .get(schedulerController.stopJob);

// router
//     .route('/destroyJob')
//     .get(schedulerController.destroyJob);

module.exports = router;
