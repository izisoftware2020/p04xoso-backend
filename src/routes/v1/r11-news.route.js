const express = require('express');
const newsController = require('../../controllers/c11-news.controller');
const validate = require('../../middlewares/validate');
const newsValidation = require('../../validations/v11-news.validation');

const router = express.Router();

router.route('/').post(newsController.createNews).get(newsController.getNews);

router.route('/:id').get(validate(newsValidation.findById), newsController.findById);

module.exports = router;
