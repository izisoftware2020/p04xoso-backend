const express = require('express');
const power645Controller = require('../../controllers/c8-power645.controller');
const power645Validate = require('../../validations/v8-power645.validation');
const validate = require('../../middlewares/validate');

const router = express.Router();

router
  .route('/')
  .post(power645Controller.createPower645)
  .get(validate(power645Validate.getPower645s), power645Controller.getPower645s);

router
  .route('/static-the-pairs-appear-the-most')
  .get(validate(power645Validate.findThePairsAppearTheMost), power645Controller.findThePairsAppearTheMost);

router
  .route('/static-frequency-of-pairs')
  .get(
    validate(power645Validate.countTheFrequencyOfOccurrenceOfPairs),
    power645Controller.countTheFrequencyOfOccurrenceOfPairs
  );

// Dò kết quả
router.route('/check').get(validate(power645Validate.checkPower645Tickets), power645Controller.checkPower645Tickets);

module.exports = router;
