const express = require('express');
const computerLotteryResultController = require('../../controllers/c6-computer_lottery_result.controller');
const validate = require('../../middlewares/validate');
const computerLotteryResultValidation = require('../../validations/v6-computer_lottery_result.validation');

const router = express.Router();

router
  .route('/')
  .post(computerLotteryResultController.createComputerLotteryResult)
  .get(
    validate(computerLotteryResultValidation.getComputerLotteryResults),
    computerLotteryResultController.getComputerLotteryResults
  );

module.exports = router;
