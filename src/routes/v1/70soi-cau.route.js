const express = require('express');
const { soiCauController } = require('../../controllers');
const validate = require('../../middlewares/validate');
const { soiCauValidation } = require('../../validations');
const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/')
  .get(auth(), soiCauController.find)
  .post(auth(), validate(soiCauValidation.create), soiCauController.create)
  .put(auth(), validate(soiCauValidation.findByIdAndUpdate), soiCauController.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(soiCauValidation.paginate), soiCauController.paginate);

router.route('/search').get(soiCauController.search);

router
  .route('/:id')
  .get(auth(), validate(soiCauValidation.findById), soiCauController.findById)
  .delete(auth(), validate(soiCauValidation.findByIdAndDelete), soiCauController.findByIdAndDelete);

module.exports = router;
