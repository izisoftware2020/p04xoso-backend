const express = require('express');
const { newsControler } = require('../../controllers');
const validate = require('../../middlewares/validate');
const { newsValidation } = require('../../validations');
const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/')
  .get(auth(), newsControler.find)
  .post(auth(), validate(newsValidation.create), newsControler.create)
  .put(auth(), validate(newsValidation.findByIdAndUpdate), newsControler.findByIdAndUpdate);

router.route('/paginate').get(auth(), validate(newsValidation.paginate), newsControler.paginate);

router
  .route('/:id')
  .get(auth(), validate(newsValidation.findById), newsControler.findById)
  .delete(auth(), validate(newsValidation.findByIdAndDelete), newsControler.findByIdAndDelete);

module.exports = router;
