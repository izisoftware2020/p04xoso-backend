const express = require('express');
const max4DController = require('../../controllers/c9-max4d.controller');
const validate = require('../../middlewares/validate');
const max4dValidation = require('../../validations/v9-max4d.validation');

const router = express.Router();

router.route('/').post(max4DController.createMax4D).get(validate(max4dValidation.getMax4Ds), max4DController.getMax4Ds);

// Dò kết quả
router.route('/check').get(validate(max4dValidation.checkMax4dTickets), max4DController.checkMax4dTickets);

module.exports = router;
