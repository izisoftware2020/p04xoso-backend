const express = require('express');
const prizeOpenScheduleController = require('../../controllers/c4-prize_open_schedule.controller');

const router = express.Router();

router
  .route('/')
  .post(prizeOpenScheduleController.createPrizeOpenSchedule)
  .get(prizeOpenScheduleController.getPrizeOpenSchedules);

module.exports = router;
