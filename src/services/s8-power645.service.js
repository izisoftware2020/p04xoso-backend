const httpStatus = require('http-status');
const { Power645 } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createPower645 = async (Power645Body) => await Power645.create(Power645Body);

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */

const queryPower645s = async (filter, options) => {
  const power645s = await Power645.paginate(filter, options);
  return power645s;
};

/**
 * Find power 645 by date
 * @param {*} dateStart this is date need find
 */
const findPower645sByDateStart = async (dateStart) => {
  dateStart = new Date(dateStart * 1000);

  return await Power645.find({ dateStart });
};

/**
 * Find power 655 by month
 * @param {date} month this is month need query
 * @author ThanhVV
 */
const findPower645sByMonthAndYear = async (month, year) => {
  const result = await Power645.find({
    dateStart: {
      $gte: new Date(year, month - 1, 1),
      $lt: new Date(year, month, 1),
    },
  }).sort({ dateStart: -1 });

  return result;
};

/**
 * find the paris appear the most most recent
 * @author ThanhVV
 */
const findThePairsAppearTheMost = async (dateStart, dateEnd) => {
  // Count number appear
  let count = 1;

  // Convert timestamp to date
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  let arrayNumber = [];
  const arrayResult = [];

  // Get result power645s in 1 month
  const result = await findPower645ByDateStartToDateEnd(dateStart, dateEnd);

  // Get array of number
  arrayNumber = getArrayNumberFromResult(result);

  arrayNumber.sort();

  // Thêm 1 số này vào data để nó duyệt hết các phần từ array
  arrayNumber.push('99');

  // Count the pairt occurrences from array of number
  for (let i = 1; i < arrayNumber.length; i++) {
    if (arrayNumber[i] == arrayNumber[i - 1]) {
      count++;
    } else {
      const object = {};
      object.name = arrayNumber[i - 1];
      object.value = count;

      count = 1;
      arrayResult.push(object);
    }
  }

  // Sort by property value
  arrayResult.sort((a, b) =>
    a.numOfOccurrences > b.numOfOccurrences ? 1 : b.numOfOccurrences > a.numOfOccurrences ? -1 : 0
  );

  return arrayResult;
};

/**
 * Statistics on the frequency of occurrence of pairs of numbers from 01 to 45 of power645
 * @param {*} dateStart this is date start need query
 * @param {*} dateEnd this is date pre need query
 * @author ThanhVV
 */
const countTheFrequencyOfOccurrenceOfPairs = async (dateStart, dateEnd) => {
  const mapCount = new Map();
  let arrayNumberOfLottery = [];
  const arrayResult = [];

  // Replace timestamp to date
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  // Get result from DB
  const result = await findPower645ByDateStartToDateEnd(dateStart, dateEnd);

  // Get 01 - 45
  const fortyFivePairs = getListOfNumber(46);

  // Get array of number from result
  arrayNumberOfLottery = getArrayNumberFromResult(result);

  // Set default value
  for (const i of fortyFivePairs) {
    mapCount.set(i, 0);
  }

  // Count array number
  for (const i of arrayNumberOfLottery) {
    if (mapCount.has(i)) {
      mapCount.set(i, mapCount.get(i) + 1);
    } else {
      mapCount.set(i, 1);
    }
  }

  // Count the frequency of occurrence of pair
  for (const i of fortyFivePairs) {
    const object = {};
    object.name = i;
    object.value = mapCount.get(i);

    arrayResult.push(object);
  }

  return arrayResult;
};

/**
 * Get set of number from result
 * @param {*} result this is result need get
 * @author ThanhVV
 */
const getArrayNumberFromResult = (result) => {
  const arrayNumbers = [];

  // Convert to JSON
  result = JSON.parse(JSON.stringify(result));

  for (const power of result) {
    const arrayNumber = power.setOfNumberWinningNumber.split(' ');

    arrayNumbers.push(...arrayNumber);
  }

  return arrayNumbers;
};

/**
 * find Power645 By Date Start End to Date Pre
 * @param {*} dateStart
 * @param {*} dateEnd
 * @author ThanhVV
 */
const findPower645ByDateStartToDateEnd = async (dateStart, dateEnd) => {
  const result = await Power645.find({})
    .where('dateStart')
    .gte(dateStart)
    .where('dateStart')
    .lte(dateEnd)
    .sort({ dateStart: -1 });

  return result;
};

/**
 * get forty five pairs of numbers
 * @author ThanhVV
 */
const getListOfNumber = (limit) => {
  const result = ['01', '02', '03', '04', '05', '06', '07', '08', '09'];

  for (let i = 10; i < limit; i++) {
    result.push(i.toString());
  }

  return result;
};

/**
 * check power 645 ticket
 * @param {*} lottery
 * @param {*} dateStart
 * @author ThanhVV
 */
const checkPower645Tickets = async (lotterys, dateStart) => {
  // result object
  const objectResult = {};

  // Result array
  const resultArray = [];

  // Convert lotterys to array
  lotterys = lotterys.split(',');

  // Get result by date start
  let resultPower645 = await findPower645sByDateStart(dateStart);

  // Convert ResultPower645 from DB to array of number
  resultPower645 = getArrayNumberFromResult(resultPower645);

  // Duyệt hết các phần tử để check từng kết quả
  for (let lottery of lotterys) {
    // Convert lottery to array
    lottery = lottery.split('-');

    // Check valid input
    if (isValidArrayNumber(lottery, 1) == false || resultPower645.length != lottery.length) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Result Error || Input not valid');
    }

    // get result after check
    const resultAfterCheck = checkPower645Ticket(lottery, resultPower645);

    resultArray.push(resultAfterCheck);
  }

  objectResult.result = resultArray;
  objectResult.resultPower645 = resultPower645;

  return objectResult;
};

/**
 * is Valid Set Of Number Of Power645
 * @param {*} lottery
 * @param {*} type 1 is check 645 2 la 655
 * @author ThanhVV
 */
const isValidArrayNumber = (lottery, type) => {
  let arrayNumber = [];

  if (type == 1) {
    // Get 01 - 45
    arrayNumber = getListOfNumber(46);
  } else {
    // Get 01 - 56
    arrayNumber = getListOfNumber(56);
  }

  for (number of lottery) {
    if (!arrayNumber.includes(number)) {
      return false;
    }
  }
  // Check is valid of set of number
  if (isDuplicateOfArrayOfNumber(lottery)) {
    return false;
  }

  return true;
};

/**
 * Check duplicate of array of number
 * @param {*} arrayOfNumber
 * @author ThanhVV
 */
const isDuplicateOfArrayOfNumber = (arrayOfNumber) => {
  const setNumber = new Set();

  for (number of arrayOfNumber) {
    setNumber.add(number);
  }

  return setNumber.size != arrayOfNumber.length;
};

/**
 * Check power 645 ticket
 * @param {array} tickets
 * @param {array} results
 */
const checkPower645Ticket = (tickets, results) => {
  // Sort result from DB and sort lottery
  tickets.sort();
  results.sort();

  // Count the same number
  const result = countTheSameNumber(tickets, results);

  if (result == 6) {
    return 'jackpot';
  }
  if (result == 5) {
    return 'firstPrize';
  }
  if (result == 4) {
    return 'secondPrize';
  }
  if (result == 3) {
    return 'thirdPrize';
  }

  return 'noPrize';
};

/**
 * Count the same number
 * @param {*} resultPower645
 * @param {*} lottery
 *  @author ThanhVV
 */
const countTheSameNumber = (lottery, resultPower645) => {
  // Count the same number
  let count = 0;
  const { length } = resultPower645;

  // Compare result
  for (let i = 0; i < length; i++) {
    let flag = false;
    for (let j = 0; j < lottery.length; j++) {
      if (resultPower645[i] == lottery[j]) {
        flag = true;
        break;
      }
    }
    if (flag == true) {
      count++;
    }
  }

  return count;
};

module.exports = {
  createPower645,
  queryPower645s,
  findPower645sByDateStart,
  findPower645sByMonthAndYear,
  findThePairsAppearTheMost,
  findPower645ByDateStartToDateEnd,
  countTheFrequencyOfOccurrenceOfPairs,
  checkPower645Tickets,
  getArrayNumberFromResult,
  getListOfNumber,
  isValidArrayNumber,
  countTheSameNumber,
};
