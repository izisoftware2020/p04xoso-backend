const { Role } = require('../models');

/**
 * Find all role
 * @returns
 */
const find = async () => {
  return Role.find();
};

/**
 * Create a role
 * @param {*} body
 * @returns
 */
const create = async (body) => {
  return Role.create(body);
};

/**
 * Update a role
 * @param {*} id
 * @param {*} body
 * @returns
 */
const findByIdAndUpdate = async (id, body) => {
  return Role.findByIdAndUpdate({ _id: id }, body);
};

/**
 * Delete a role
 * @param {*} ids
 * @returns
 */
const findByIdAndDelete = async (ids) => {
  return Role.deleteMany({ _id: { $in: ids.split(',') } });
};

/**
 * Find a role by id
 * @param {*} id
 * @returns
 */
const findById = async (id) => {
  return Role.findById({ _id: id });
};

/**
 * Paginate
 * @param {*} filter
 * @param {*} options
 * @returns
 */
const paginate = async (filter, options) => {
  return Role.paginate(filter, options);
};

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
