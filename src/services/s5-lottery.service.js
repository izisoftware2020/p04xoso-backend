const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { Lottery } = require('../models');
const Constant = require('../utils/constant');

/**
 * Create a Lottery
 * @param {*} LotteryBody
 * @returns  {Promise <Lottery>}
 */
const createLottery = async (LotteryBody) => {
  return await Lottery.create(LotteryBody);
};

/**
 * Delete all Data
 * @returns records
 */
const deleteAll = async () => {
  const records = await Lottery.deleteMany();
  return await records;
};

/**
 * Query for Lottery
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryLotterys = async (filter, options) => {
  const Lotterys = await Lottery.paginate(filter, options);
  return Lotterys;
};

/**
 * Find Lotterys By IdRegion And DateStart
 * @param {*} idRegion
 * @param {*} dateStart
 * @author ThanhVV
 */
const findLotterysByIdRegionAndDateStart = async (idRegion, dateStart, limit) => {
  // Params
  var dateStart = new Date(dateStart * 1000);
  const dateEnd = new Date();
  dateEnd.setDate(dateStart.getDate() - limit);

  // Query data
  const result = await Lottery.find({})
    .where('dateStart')
    .gte(dateEnd)
    .where('dateStart')
    .lte(dateStart)
    .where('region')
    .equals(idRegion)
    .populate('region')
    .populate('province')
    .sort({ dateStart: -1 });

  return result;
};

/**
 * Find lottery by date start to date end and idProvince
 * @param {*} idProvince
 * @param {*} dateStart
 */
const findLotteryByDateStartToDateEndAndIdProvince = async (idProvince, dateStart, dateEnd) => {
  // Query data
  const result = await Lottery.find({})
    .where('dateStart')
    .gte(dateStart)
    .where('dateStart')
    .lte(dateEnd)
    .where('province')
    .equals(idProvince)
    .populate('region')
    .populate('province')
    .sort({ dateStart: -1 });

  return result;
};

/**
 * Find lotterys by id province
 * @param {*} idProvince this IdProvince
 * @param {*} options
 */
const findLotterysByIdProvince = async (idProvince, options) => {
  const lotterys = await Lottery.paginate({ province: idProvince }, options);

  return lotterys;
};

/**
 * Find lotterys by idprovince and date start
 * @param {*} idProvince this is province need query
 * @param {*} dateStart this is date start query
 * @author ThanhVV
 */
const findLotteryByIdProvinceAndDateStart = async (dateStart, idProvince) => {
  const lottery = await Lottery.findOne({ dateStart, province: idProvince });

  return lottery;
};

/**
 * Check lottery tickets
 * @param {*} dateStart this is date query
 * @param {*} idProvince this is province need query
 * @param {*} lottery this is lottery need query
 * @author ThanhVV
 */
const checkLotteryTickets = async (dateStart, idProvince, lotteryParams) => {
  // Convert timestamp to date
  const dateQuery = new Date(dateStart * 1000);

  // Get result by query
  const lottery = await findLotteryByIdProvinceAndDateStart(dateQuery, idProvince);

  if (lottery == null || lotteryParams.length > 6 || lotteryParams.length < 5) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Input not valid');
  }

  // Replace IdProvince, 1 is North side else south side
  idProvince = replaceIdProvince(idProvince);

  return getLotteryResultAfterChecking(lottery, lotteryParams, idProvince);
};

/**
 * Replace idProvince
 * Nếu IdProvince = ID_PROVINCE_NORTH_SIDE thì IdProvince chuyển thành 1, ngược lại thành 2
 */
const replaceIdProvince = (idProvince) => {
  if (idProvince == Constant.ID_PROVINCE_NORTH_SIDE) {
    return 1;
  }
  return 2;
};

/**
 * Get lottery result after checking
 * @param {*} property this is property of lottery from eightPrize to specialPrize
 * @param {*} lotteryParams this is lottery need query
 * @author ThanhVV
 */
const getLotteryResultAfterChecking = async (lottery, lotteryParams, province) => {
  // Hashmap key is prizenumber value is result from DB
  let mapData = new Map();

  // Get HashMap data from lottery
  mapData = getMapDataFromLottery(lottery);

  // Get result from map data after checking
  return getResultFromMapDataAfterChecking(mapData, lotteryParams, province);
};

/**
 * Get Map Data From Lottery
 * Example {speacial : 55555}
 * @param {*} lottery this lottery need querys
 * @author ThanhVV
 */
const getMapDataFromLottery = (lottery) => {
  const mapData = new Map();

  // Convert data to JSON
  lottery = lottery.toJSON();

  for (const p in lottery) {
    // Check valid prize number
    if (isValidPrizeNumber(p)) {
      mapData.set(p, lottery[p]);
    }
  }

  return mapData;
};

/**
 * Get result from map data after checking
 * @param {*} mapData this is map data key is prize number value is result
 * @param {*} lotteryParam this is lottery param
 * @returns  result
 */
const getResultFromMapDataAfterChecking = (mapData, lotteryParam, province) => {
  const result = [];
  mapData.forEach((value, keys) => {
    const object = {};
    object.name = keys;
    object.value = getNumberOfPrizesWon(lotteryParam, keys, value, province);

    result.push(object);
  });

  // Giải khuyến khích
  const object = {};
  object.name = 'consolationPrizes';
  object.value = isWinConsolationPrize(lotteryParam, mapData.get('specialPrize'), province) ? 1 : 0;
  result.push(object);

  // Giải đặt biệt phụ của miền trung hoặc miền nam
  if (province != Constant.ID_PROVINCE_NORTH_SIDE) {
    const objectExtraSpecialPrize = {};
    objectExtraSpecialPrize.name = 'extraSpecialPrize';
    objectExtraSpecialPrize.value = isWinExtraSpecialPrize(lotteryParam, mapData.get('specialPrize')) ? 1 : 0;
    result.push(objectExtraSpecialPrize);
  }

  return result;
};

/**
 * Is win consolation prize
 * @param {*} lotteryParam
 * @param {*} specialPrize
 * @param {*} province
 * @author ThanhVV
 */
const isWinConsolationPrize = (lotteryParam, specialPrize, province) => {
  // Count the same number
  let count = 0;
  const { length } = lotteryParam;

  if (lotteryParam.length != specialPrize.length) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Input not valid');
  }

  // Check Southside
  if (province == 1) {
    if (lotteryParam != specialPrize) {
      return lotteryParam.substring(3, 5) == specialPrize.substring(3, 5);
    }
    return false;
  }
  const flag = lotteryParam.charAt(0) == specialPrize.charAt(0);
  for (let i = 1; i < length; i++) {
    if (lotteryParam.charAt(i) == specialPrize.charAt(i)) {
      count++;
    }
  }
  // Trúng chữ số đầu tiên và sai 1 trong 5 chử số còn lại
  return count == 4 && flag;
};

/**
 * Is Win Extra Special Prize
 */
const isWinExtraSpecialPrize = (lotteryParam, specialPrize) => {
  const { length } = lotteryParam;
  // Sai 5 chử đầu, trúng 5 chử số cuối so với giải đặc biệt
  if (lotteryParam != specialPrize && lotteryParam.substring(1, length) == specialPrize.substring(1, length)) {
    return true;
  }
  return false;
};

/**
 * Check valide prize number
 * @param {*} prizeNumber
 * @returns true if right else false
 * @author ThanhVV
 */
const isValidPrizeNumber = (prizeNumber) => {
  return (
    prizeNumber === 'eighthPrize' ||
    prizeNumber === 'seventhPrize' ||
    prizeNumber === 'sixthPrize' ||
    prizeNumber === 'fifthPrize' ||
    prizeNumber === 'fourthPrize' ||
    prizeNumber === 'thirdPrize' ||
    prizeNumber === 'secondPrize' ||
    prizeNumber === 'firstPrize' ||
    prizeNumber === 'specialPrize'
  );
};

/**
 * Check win the price
 * @param {*} lotteryParams
 * @param {*} prizeNumber
 * @param {*} lotteryResults
 * @param {*} idProvince
 * @author ThanhVV
 */
const getNumberOfPrizesWon = (lotteryParams, prizeNumber, lotteryResults, province) => {
  let countResult = 0;

  // Get number of lottery need commpare from number prize
  const value = getNumbersOfLotteryFromPrizeNumber(lotteryParams, prizeNumber, province);

  const results = lotteryResults.split(' ');

  // Compare Result ...
  for (i of results) {
    if (i == value) {
      countResult++;
    }
  }

  return countResult;
};

/**
 * Get numbers of lottery from number prize
 * @param {*} lotteryParams this is lotteryParams need resolve
 * @param {*} prizeNumber this is prizeNumber
 * @param {*} province this is province // 1 là miền bắc ngược lại tỉnh khác
 * @author ThanhVV
 *
 */
const getNumbersOfLotteryFromPrizeNumber = (lotteryParams, prizeNumber, province) => {
  const lengthLottery = lotteryParams.length;

  // kiểm tra khu vực phía bắc
  if (province == 1) {
    if (prizeNumber === 'seventhPrize') {
      return lotteryParams.substring(lengthLottery - 2, lengthLottery);
    }
    if (prizeNumber == 'sixthPrize') {
      return lotteryParams.substring(lengthLottery - 3, lengthLottery);
    }
    if (prizeNumber == 'fifthPrize' || prizeNumber == 'fourthPrize') {
      return lotteryParams.substring(lengthLottery - 4, lengthLottery);
    }
    if (prizeNumber == 'thirdPrize' || prizeNumber == 'secondPrize' || prizeNumber == 'firstPrize') {
      return lotteryParams.substring(lengthLottery - 5, lengthLottery);
    }
    return lotteryParams;

    // kiểm tra khu vực ở phia khác
  }
  if (prizeNumber == 'eighthPrize') {
    return lotteryParams.substring(lengthLottery - 2, lengthLottery);
  }
  if (prizeNumber == 'seventhPrize') {
    return lotteryParams.substring(lengthLottery - 3, lengthLottery);
  }
  if (prizeNumber == 'sixthPrize') {
    return lotteryParams.substring(lengthLottery - 4, lengthLottery);
  }
  if (prizeNumber == 'fifthPrize') {
    return lotteryParams.substring(lengthLottery - 4, lengthLottery);
  }
  if (
    prizeNumber == 'fourthPrize' ||
    prizeNumber == 'thirdPrize' ||
    prizeNumber == 'secondPrize' ||
    prizeNumber == 'firstPrize'
  ) {
    return lotteryParams.substring(lengthLottery - 5, lengthLottery);
  }
  return lotteryParams;
};

/**
 * find lotterys by date query
 * @param {*} dateStart this is date need query
 * @author ThanhVV
 */
const findLotterysByDateStart = async (dateStart) => {
  var dateStart = new Date(dateStart * 1000);

  return await Lottery.find({ dateStart }).populate('region').populate('province');
};

/**
 * Count the pairs of number that apprear most
 * @param {*} dateStart
 * @param {*} dateEnd
 * @param {*} idRegionOrIdProvince
 * @param {*} type if type = 1 then find by id region else find by id province
 */
const statisThePairsOfNumberThatAppearMost = async (idRegionOrIdProvince, dateStart, dateEnd, type) => {
  // Convert timestamp to date start
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);
  let resultLotterys = '';

  // Find lottery by date start and date pre and sort desc by id region or idprovince
  if (type == 1) {
    resultLotterys = await findLotteryByDateStartToDateEndAndIdRegion(idRegionOrIdProvince, dateStart, dateEnd);
  } else {
    resultLotterys = await findLotteryByDateStartToDateEndAndIdProvince(idRegionOrIdProvince, dateStart, dateEnd);
  }

  // Replace IdProvince, 1 is North side else south sidereplaceIdRegion
  idRegionOrIdProvince = replaceIdRegion(idRegionOrIdProvince);

  // Get Array Result and date appear by id region from Lottery
  resultLotterys = await getArrayResultAndDateAppearByIdRegion(resultLotterys, idRegionOrIdProvince);

  // Get arrat last two digits of special prize and eight prize or seventh prize by idRegion
  let resultArrayNumber = getArrayNumberFromResultLotteryByIdRegionOrIdProvince(resultLotterys, idRegionOrIdProvince);

  // This is map, key is number, value is appear of number. example {3 => 1}
  const mapCount = countAppearOfNumber(resultArrayNumber);

  // Convert mapcount to array object
  resultArrayNumber = getArrayNumberObjectFromMap(mapCount);

  // Add the latest appearance date into result
  resultArrayNumber = addTheLatestAppearanceDateIntoResult(resultArrayNumber, resultLotterys, idRegionOrIdProvince, 2);

  // Add percent appear into result lottery
  resultArrayNumber = addPercenAppearIntoResultArray(resultArrayNumber);

  // Add last appearance date
  resultArrayNumber = addLastAppearanceDate(resultArrayNumber);

  // Sort DESC Count appearance of number
  resultArrayNumber = resultArrayNumber.sort((a, b) => (a.value > b.value ? 1 : b.value > a.value ? -1 : 0));

  return resultArrayNumber;
};

/**
 * Find Lottery traditional By Date Start End to Date Pre
 * @param {date} dateStart
 * @param {datePre} datePre
 * @author ThanhVV
 */
const findLotteryByDateStartToDateEndAndIdRegion = async (idRegion, dateStart, dateEnd) => {
  const result = await Lottery.find({})
    .where('dateStart')
    .gt(dateStart)
    .where('dateStart')
    .lte(dateEnd)
    .where('region')
    .equals(idRegion)
    .sort({ dateStart: -1 });

  return result;
};

/**
 * Get the pairs of number and date appear from lottery
 * @param {*} lottery this is lotterys
 * @param {*} idRegion idProvince = 1 is NorthSide else  Center Side or South side
 */
const getArrayResultAndDateAppearByIdRegion = async (resultLotterys, idRegion) => {
  const results = [];

  // Convert to JSON
  resultLotterys = JSON.parse(JSON.stringify(resultLotterys));

  for (const lottery of resultLotterys) {
    const object = {};
    object.specialPrize = lottery.specialPrize;
    object.firstPrize = lottery.firstPrize;
    object.secondPrize = lottery.secondPrize;
    object.thirdPrize = lottery.thirdPrize;
    object.fourthPrize = lottery.fourthPrize;
    object.fifthPrize = lottery.fifthPrize;
    object.sixthPrize = lottery.sixthPrize;
    object.seventhPrize = lottery.seventhPrize;
    object.dateAppear = lottery.dateStart;

    // Check IdRegion if = 1 is NorthSide else SouthSide
    if (idRegion == 2) {
      object.eighthPrize = lottery.eighthPrize;
    }

    results.push(object);
  }
  return results;
};

/**
 * Replace IdRegion
 * Nếu IdRegion = ID_PROVINCE_NORTH_SIDE thì IdRegion chuyển thành 1, ngược lại thành 2
 */
const replaceIdRegion = (idRegion) => {
  if (idRegion == Constant.ID_REGION_NORTH_SIDE || idRegion == Constant.ID_PROVINCE_NORTH_SIDE) {
    return 1;
  }
  return 2;
};

/**
 * Get array number from result lottery by id idRegion
 * @param {*} resultLotterys
 * @param {*} idRegionOrProvince
 * @author ThanhVV
 */
const getArrayNumberFromResultLotteryByIdRegionOrIdProvince = (resultLotterys, idRegionOrProvince) => {
  const results = [];

  for (const result of resultLotterys) {
    const arrayOfTwoDigitOfSpecialPrize = getArrayLastTwoDigitOfPrizeNumber(result.specialPrize);
    const arrayOfTwoDigitOfFirstPrize = getArrayLastTwoDigitOfPrizeNumber(result.firstPrize);
    const arrayOfTwoDigitOfSecondPrize = getArrayLastTwoDigitOfPrizeNumber(result.secondPrize);
    const arrayOfTwoDigitOfThirdPrize = getArrayLastTwoDigitOfPrizeNumber(result.thirdPrize);
    const arrayOfTwoDigitOfFourthPrize = getArrayLastTwoDigitOfPrizeNumber(result.fourthPrize);
    const arrayOfTwoDigitOfFifthPrize = getArrayLastTwoDigitOfPrizeNumber(result.fifthPrize);
    const arrayOfTwoDigitOfSixthPrize = getArrayLastTwoDigitOfPrizeNumber(result.sixthPrize);
    const arrayOfTwoDigitOfSeventhPrize = getArrayLastTwoDigitOfPrizeNumber(result.seventhPrize);

    results.push(...arrayOfTwoDigitOfSpecialPrize);
    results.push(...arrayOfTwoDigitOfFirstPrize);
    results.push(...arrayOfTwoDigitOfSecondPrize);
    results.push(...arrayOfTwoDigitOfThirdPrize);
    results.push(...arrayOfTwoDigitOfFourthPrize);
    results.push(...arrayOfTwoDigitOfFifthPrize);
    results.push(...arrayOfTwoDigitOfSixthPrize);
    results.push(...arrayOfTwoDigitOfSeventhPrize);

    // kiểm tra nếu id = 1 thì miền bắc là 2 miền trung hoặc miền nam
    if (idRegionOrProvince == 2) {
      const arrayOfTwoDigitDigitOfEighthPrize = getArrayLastTwoDigitOfPrizeNumber(result.eighthPrize);
      results.push(...arrayOfTwoDigitDigitOfEighthPrize);
    }
  }
  return results;
};

/**
 * Get last two digit of prize number
 * @param {*} prizeNumber
 * @author ThanhVV
 */
const getArrayLastTwoDigitOfPrizeNumber = (prizeNumber) => {
  const arrayResult = [];

  const arrayPrizeNumber = prizeNumber.split(' ');

  for (const value of arrayPrizeNumber) {
    arrayResult.push(getLastTwoDigitsOfStringResult(value));
  }

  return arrayResult;
};

/**
 * Get last two digits of string
 * @param {*} stringResult
 * @autho ThanhVV
 */
const getLastTwoDigitsOfStringResult = (stringResult) => {
  const { length } = stringResult;

  return stringResult.substring(length - 2, length);
};

/**
 * Count appear of number
 * @param {*} arrayNumber this is array number need count
 * @author ThanhVV
 */
const countAppearOfNumber = (arrayNumber) => {
  const mapCount = new Map();

  for (const value of arrayNumber) {
    if (mapCount.has(value)) {
      mapCount.set(value, mapCount.get(value) + 1);
    } else {
      mapCount.set(value, 1);
    }
  }

  return mapCount;
};

/**
 * Get Array Number Object FromMap
 * @param {*} mapResult
 * @author ThanhVV
 */
const getArrayNumberObjectFromMap = (mapResult) => {
  const arrayResult = [];

  mapResult.forEach((value, key) => {
    const object = {};
    object.name = key;
    object.value = value;

    arrayResult.push(object);
  });

  return arrayResult;
};

/**
 * Add the latest appearance date into result
 * @param {*} arrayResult this is array result need add date
 * @param {*} arrayLottery this is array lottery have date and value
 * @param {*} idRegion this is id region
 * @param {*} type 1 thì check giải đặc biệt ngược lại check tất cả
 * @author ThanhVV
 */
const addTheLatestAppearanceDateIntoResult = (arrayResults, arrayLotterys, idRegion, type) => {
  for (const arrayResult of arrayResults) {
    // This is value of array result
    const value = arrayResult.name;
    for (const arrayLottery of arrayLotterys) {
      // If type = 1 thì check đặt biệt ngược lại là check tất cả
      if (type == 1) {
        const arrayOfTwoDigitOfSpecialPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.specialPrize);
        // Check exist of array
        if (arrayOfTwoDigitOfSpecialPrize.includes(value)) {
          arrayResult.dateAppear = arrayLottery.dateAppear;
          break;
        }
      } else {
        const arrayOfTwoDigitOfSpecialPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.specialPrize);
        const arrayOfTwoDigitOfFirstPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.firstPrize);
        const arrayOfTwoDigitOfSecondPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.secondPrize);
        const arrayOfTwoDigitOfThirdPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.thirdPrize);
        const arrayOfTwoDigitOfFourthPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.fourthPrize);
        const arrayOfTwoDigitOfFifthPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.fifthPrize);
        const arrayOfTwoDigitOfSixthPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.sixthPrize);
        const arrayOfTwoDigitOfSeventhPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.seventhPrize);

        // Check exist of array
        if (
          arrayOfTwoDigitOfSpecialPrize.includes(value) ||
          arrayOfTwoDigitOfFirstPrize.includes(value) ||
          arrayOfTwoDigitOfSecondPrize.includes(value) ||
          arrayOfTwoDigitOfThirdPrize.includes(value) ||
          arrayOfTwoDigitOfFourthPrize.includes(value) ||
          arrayOfTwoDigitOfFifthPrize.includes(value) ||
          arrayOfTwoDigitOfSixthPrize.includes(value) ||
          arrayOfTwoDigitOfSeventhPrize.includes(value)
        ) {
          arrayResult.dateAppear = arrayLottery.dateAppear;
          break;
        }

        // Kiểm tra idRegion là miền nam hoặc miền trung
        if (idRegion == 2) {
          const arrayOfTwoDigitDigitOfEighthPrize = getArrayLastTwoDigitOfPrizeNumber(arrayLottery.eighthPrize);
          if (arrayOfTwoDigitDigitOfEighthPrize.includes(value)) {
            arrayResult.dateAppear = arrayLottery.dateAppear;
            break;
          }
        }
      }
    }
  }

  return arrayResults;
};

/**
 * Statis appear of lottery winning from result
 * @param {*} lottery this is lottery need check, lottery length > 1  && < 7
 * @param {*} idProvinces this is idProvinces need check
 * @param {*} dateStart
 * @param {*} dateEnd
 * @author ThanhVV
 */
const statisAppearOfNumbersWinningFromResult = async (lottery, idProvince, dateStart, dateEnd) => {
  // Convert timestamp to date
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  // Check valid number of lottery
  if (!isValidNumbersOfLottery(lottery)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Length lottery not valid');
  }
  // Get result of lotterys
  let resultOfLotterys = await findLotteryByDateStartToDateEndAndIdProvince(idProvince, dateStart, dateEnd);

  // Replace IdProvince, 1 is North side else south side
  idProvince = replaceIdProvince(idProvince);

  // Get wining prize from result of lottery
  resultOfLotterys = getWinningPrizesFromResultOfLottery(idProvince, resultOfLotterys);

  // Get array number prize have number lottery
  return getArrayNumberPrizeHaveNumberLottery(lottery, resultOfLotterys);
};

/**
 * Check valid of number lottery
 * @param {*} numbersOfLottery
 */
const isValidNumbersOfLottery = (numbersTickets) => {
  return numbersTickets.length > 1 && numbersTickets.length < 7;
};

/**
 * Get winining prizes from result of lottery
 * @param {*} numbersOfLottery
 * @param {*} resultOfLottery
 * @author ThanhVV
 */
const getWinningPrizesFromResultOfLottery = (idProvince, resultOfLotterys) => {
  /*
        Đây là kết quả mà sao khi query ta đã loại bỏ các thuột tính thừa để lại
        các property is specialPrize, firstPrize, secondPrize...
    */
  const results = [];

  // Convert to JSON
  resultOfLotterys = JSON.parse(JSON.stringify(resultOfLotterys));

  for (const lottery of resultOfLotterys) {
    // Get Array Result And Date Appear By Id Province
    results.push(getArrayResultAndDateAppearByIdProvince(lottery, idProvince));
  }

  return results;
};

/**
 * Get Array Result And Date Appear By Id Province from lottery
 * @param {*} lottery this is lotterys
 * @param {*} idProvince idProvince = 1 is NorthSide else  Center Side or South side
 */
const getArrayResultAndDateAppearByIdProvince = (lottery, idProvince) => {
  // Convert to JSON
  lottery = JSON.parse(JSON.stringify(lottery));

  const object = {};
  object.specialPrize = lottery.specialPrize;
  object.firstPrize = lottery.firstPrize;
  object.secondPrize = lottery.secondPrize;
  object.thirdPrize = lottery.thirdPrize;
  object.fourthPrize = lottery.fourthPrize;
  object.fifthPrize = lottery.fifthPrize;
  object.sixthPrize = lottery.sixthPrize;
  object.seventhPrize = lottery.seventhPrize;
  object.dateAppear = lottery.dateStart;

  // Check IdRegion if = 1 is NorthSide else SouthSide
  if (idProvince == 2) {
    object.eighthPrize = lottery.eighthPrize;
  }

  return object;
};

/**
 * Get array number prize have number lottery
 * @param {*} lottery this is lottery params need query
 * @param {*} resultOfLotterys this is result of lottery
 * @author ThanhVV
 */
const getArrayNumberPrizeHaveNumberLottery = (lottery, resultOfLotterys) => {
  // Convert to JSON
  resultOfLotterys = JSON.parse(JSON.stringify(resultOfLotterys));

  const arrayResult = [];

  for (const value of resultOfLotterys) {
    // Kiểm tra giải đặt biệt
    if (isNumberPrizeHaveNumberLottery(lottery, value.specialPrize)) {
      const object = {};
      object.name = 'specialPrize';
      object.value = value.specialPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    // Kiểm tra giải nhất
    if (isNumberPrizeHaveNumberLottery(lottery, value.firstPrize)) {
      const object = {};
      object.name = 'firstPrize';
      object.value = value.firstPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    // kiểm tra giải nhì
    if (isNumberPrizeHaveNumberLottery(lottery, value.secondPrize)) {
      const object = {};
      object.name = 'secondPrize';
      object.value = value.secondPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    // kiểm tra giải 3
    if (isNumberPrizeHaveNumberLottery(lottery, value.thirdPrize)) {
      const object = {};
      object.name = 'thirdPrize';
      object.value = value.thirdPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    // kiểm tra giải 4
    if (isNumberPrizeHaveNumberLottery(lottery, value.fourthPrize)) {
      const object = {};
      object.name = 'fourthPrize';
      object.value = value.fourthPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    // kiểm tra giải 5
    if (isNumberPrizeHaveNumberLottery(lottery, value.fifthPrize)) {
      const object = {};
      object.name = 'fifthPrize';
      object.value = value.fifthPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    // kiểm tra giải 6
    if (isNumberPrizeHaveNumberLottery(lottery, value.sixthPrize)) {
      const object = {};
      object.name = 'sixthPrize';
      object.value = value.sixthPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    // kiểm tra giải 7
    if (isNumberPrizeHaveNumberLottery(lottery, value.seventhPrize)) {
      const object = {};
      object.name = 'seventhPrize';
      object.value = value.seventhPrize;
      object.dateAppear = value.dateAppear;
      arrayResult.push(object);
    }
    if (isTheInputStringValid(lottery.eighthPrize)) {
      if (isNumberPrizeHaveNumberLottery(lottery, value.eighthPrize)) {
        const object = {};
        object.name = 'eighthPrize';
        object.value = value.eighthPrize;
        object.dateAppear = value.dateAppear;
        arrayResult.push(object);
      }
    }
  }
  return arrayResult;
};

/**
 * Check number prize have number lottery
 * @param {*} lottery
 * @param {*} numberPrize  vd: {special : "123456"}
 */
const isNumberPrizeHaveNumberLottery = (lottery, numberPrize) => {
  let flag = false;
  const { length } = lottery;

  // Convert string to array
  const arrayNumber = numberPrize.split(' ');

  for (let value of arrayNumber) {
    value = value.substring(value.length - length, value.length);
    // Compare result
    if (value == lottery) {
      flag = true;
    }
  }
  return flag;
};

/**
 * Static loto gan
 * @param {*} idProvince this is idProvince
 * @param {*} lottery this is number lottery need check length > 1 < 7
 * @param {*} type có 3 loại theo 1 là theo lô,2 là theo đầu đuôi,3 là đặc biệt
 * @param {*} dateStart this is dateStart
 * @param {*} dateEnd this is dateEnd
 * @author ThanhVV
 */
const statisLotoGan = async (type, idProvince, lottery, dateStart, dateEnd) => {
  let resultObject = {};

  // Convert timestamp to date
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  // Check type valid
  if (type < 1 && type > 3) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Type not valid');
  }
  // Check valid number of lottery
  if (!isValidNumbersOfLottery(lottery)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Length lottery not valid');
  }
  // Get resultLotterys
  const resultLotterys = await findLotteryByDateStartToDateEndAndIdProvince(idProvince, dateStart, dateEnd);

  // Replace IdProvince, 1 is North side else south side
  idProvince = replaceIdProvince(idProvince);

  // Kiểm tra nếu type là 1 thì kiểm tra trong lô, 2 kiểm tra đầu đuôi, 3 kiểm tra đặt biệt
  if (type == 1) {
    resultObject = staticLotoTheoLo(lottery, resultLotterys, idProvince);
  } else if (type == 2) {
    resultObject = staticLotoTheoDauDuoi(lottery, resultLotterys, idProvince);
  } else if (type == 3) {
    resultObject = staticLotoGiaiDatBiet(lottery, resultLotterys, idProvince);
  }

  return resultObject;
};

/**
 * Static loto theo lô
 * @param {*} lottery
 * @param {*} resultLotterys
 * @param {*} idProvince
 */
const staticLotoTheoLo = (lottery, resultLotterys, idProvince) => {
  const resultObject = {};
  let countApprove = 0;
  let countNotApprove = 0;
  let maxCountNotApprove = 0;
  let lastAppearanceDate = '';

  // Convert to JSON
  resultLotterys = JSON.parse(JSON.stringify(resultLotterys));

  for (const value of resultLotterys) {
    let checkNotApprove = true;

    // Kiểm tra giải nhất
    if (isNumberPrizeHaveNumberLottery(lottery, value.firstPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // kiểm tra giải nhì
    if (isNumberPrizeHaveNumberLottery(lottery, value.secondPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // kiểm tra giải 3
    if (isNumberPrizeHaveNumberLottery(lottery, value.thirdPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // kiểm tra giải 4
    if (isNumberPrizeHaveNumberLottery(lottery, value.fourthPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // kiểm tra giải 5
    if (isNumberPrizeHaveNumberLottery(lottery, value.fifthPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // kiểm tra giải 6
    if (isNumberPrizeHaveNumberLottery(lottery, value.sixthPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // Nếu IdProvince = 2 thì kiểm tra giải 7
    if (idProvince == 2) {
      if (isNumberPrizeHaveNumberLottery(lottery, value.seventhPrize)) {
        countApprove++;
        checkNotApprove = false;
      }
    }
    // Check checkNotApprove and busines logic of  maxCountNotApprove
    if (checkNotApprove == true) {
      countNotApprove++;
    } else if (countNotApprove > maxCountNotApprove) {
      maxCountNotApprove = countNotApprove;
      countNotApprove = 0;
    }
    // Update date approve
    if (countApprove > 0 && lastAppearanceDate == '') {
      lastAppearanceDate = value.dateStart;
    }
  }

  if (countNotApprove > maxCountNotApprove) {
    maxCountNotApprove = countNotApprove;
  }

  resultObject.maxGan = maxCountNotApprove;
  resultObject.value = countApprove;
  resultObject.lastAppearanceDate = lastAppearanceDate;

  return resultObject;
};

/**
 * Static loto theo đầu đuôi
 * @param {*} lottery
 * @param {*} resultLotterys
 * @param {*} idProvince
 */
const staticLotoTheoDauDuoi = (lottery, resultLotterys, idProvince) => {
  const resultObject = {};
  let countApprove = 0;
  let countNotApprove = 0;
  let maxCountNotApprove = 0;
  let lastAppearanceDate = '';

  // Convert to JSON
  resultLotterys = JSON.parse(JSON.stringify(resultLotterys));

  for (const value of resultLotterys) {
    let checkNotApprove = true;
    // Kiểm tra giải đặt biệt
    if (isNumberPrizeHaveNumberLottery(lottery, value.specialPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // Kiểm tra id vùng miền, nếu 1 là miền bắc, 2 là miền trung hoặc là miền nam
    if ((idProvince = 1)) {
      // Kiểm tra giải đặt bảy
      if (isNumberPrizeHaveNumberLottery(lottery, value.seventhPrize)) {
        countApprove++;
        checkNotApprove = false;
      }
    } else {
      // Kiểm tra giải đặt 8
      if (isNumberPrizeHaveNumberLottery(lottery, value.seventhPrize)) {
        countApprove++;
        checkNotApprove = false;
      }
    }

    // Check checkNotApprove and busines logic of  maxCountNotApprove
    if (checkNotApprove == true) {
      countNotApprove++;
    } else if (countNotApprove > maxCountNotApprove) {
      maxCountNotApprove = countNotApprove;
      countNotApprove = 0;
    }
    // Update date approve
    if (countApprove > 0 && lastAppearanceDate == '') {
      lastAppearanceDate = value.dateStart;
    }
  }

  resultObject.maxGan = maxCountNotApprove;
  resultObject.value = countApprove;
  resultObject.lastAppearanceDate = lastAppearanceDate;

  return resultObject;
};

/**
 * Static loto theo đặt biệt
 * @param {*} resultLottery
 */
const staticLotoGiaiDatBiet = (lottery, resultLotterys) => {
  const resultObject = {};
  let countApprove = 0;
  let countNotApprove = 0;
  let maxCountNotApprove = 0;
  let lastAppearanceDate = '';

  // Convert to JSON
  resultLotterys = JSON.parse(JSON.stringify(resultLotterys));

  for (const value of resultLotterys) {
    let checkNotApprove = true;
    // Kiểm tra giải đặt biệt
    if (isNumberPrizeHaveNumberLottery(lottery, value.specialPrize)) {
      countApprove++;
      checkNotApprove = false;
    }
    // Check checkNotApprove and busines logic of  maxCountNotApprove
    if (checkNotApprove == true) {
      countNotApprove++;
    } else if (countNotApprove > maxCountNotApprove) {
      maxCountNotApprove = countNotApprove;
      countNotApprove = 0;
    }
    // Update date approve
    if (countApprove > 0 && lastAppearanceDate == '') {
      lastAppearanceDate = value.dateStart;
    }
  }
  resultObject.maxGan = maxCountNotApprove;
  resultObject.value = countApprove;
  resultObject.lastAppearanceDate = lastAppearanceDate;

  return resultObject;
};

/**
 * Static loto with the last two digits
 * @param {*} idProvince
 * @param {*} dateStart
 * @param {*} dateEnd
 * @author ThanhVV
 */
const statisLotoWithTheLastTwoDigits = async (idProvince, dateStart, dateEnd) => {
  // Convert timestamp to date
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  // Get lottery result
  const resultLotterys = await findLotteryByDateStartToDateEndAndIdProvince(idProvince, dateStart, dateEnd);

  //  Get total of two last digits from result lotterys
  return getTotalOfTwoLastDigitsFromResultLottery(resultLotterys);
};

/**
 * Get total of two last digits from result lotterys
 * @param {*} resultLotterys
 */
const getTotalOfTwoLastDigitsFromResultLottery = (resultLotterys) => {
  const arrayResult = [];

  // Convert to JSON
  resultLotterys = JSON.parse(JSON.stringify(resultLotterys));

  for (const value of resultLotterys) {
    const object = {};

    const specialPrize = {
      result: value.specialPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.specialPrize).join(' '),
    };

    const firstPrize = {
      result: value.firstPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.firstPrize).join(' '),
    };

    const secondPrize = {
      result: value.secondPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.secondPrize).join(' '),
    };

    const thirdPrize = {
      result: value.thirdPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.thirdPrize).join(' '),
    };

    const fourthPrize = {
      result: value.fourthPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.fourthPrize).join(' '),
    };

    const fifthPrize = {
      result: value.fifthPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.fifthPrize).join(' '),
    };
    const sixthPrize = {
      result: value.sixthPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.sixthPrize).join(' '),
    };

    const seventhPrize = {
      result: value.seventhPrize,
      totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.seventhPrize).join(' '),
    };

    if (isTheInputStringValid(value.eighthPrize)) {
      const eighthPrize = {
        result: value.eighthPrize,
        totalLastDigits: getTotalOfTwoLastDigitsFromPrizeNumber(value.eighthPrize).join(' '),
      };
      object.eighthPrize = eighthPrize;
    }

    object.specialPrize = specialPrize;
    object.firstPrize = firstPrize;
    object.secondPrize = secondPrize;
    object.thirdPrize = thirdPrize;
    object.fourthPrize = fourthPrize;
    object.fifthPrize = fifthPrize;
    object.sixthPrize = sixthPrize;
    object.seventhPrize = seventhPrize;
    object.dateStart = value.dateStart;

    arrayResult.push(object);
  }
  return arrayResult;
};

/**
 * Get total of two last digits from prizeNumber
 * @param {*} prizeNumber example {"special":"123456"}
 */
const getTotalOfTwoLastDigitsFromPrizeNumber = (prizeNumbers) => {
  const arrayResult = [];
  let sumDigits = '';

  // Convert string  to array
  prizeNumbers = prizeNumbers.split(' ');

  for (const value of prizeNumbers) {
    // Get last two digits of string result
    twoLastDigits = getLastTwoDigitsOfStringResult(value);

    // Sum of array number
    sumDigits = sumOfArrayNumber(twoLastDigits);

    arrayResult.push(sumDigits);
  }
  return arrayResult;
};

/**
 * sum Of array number
 * @param {*} arrayNumber
 * @returns
 */
const sumOfArrayNumber = (arrayNumber) => {
  if (arrayNumber == '10') {
    return '0';
  }

  if (arrayNumber.length == 1) {
    return arrayNumber;
  }

  let sum = 0;

  // Conver to array
  arrayNumber = arrayNumber.split('');

  for (const value of arrayNumber) {
    sum += parseInt(value);
  }

  arrayNumber = sum.toString();

  return arrayNumber.substring(arrayNumber.length - 2 + arrayNumber.length - 1);
};

/**
 * statis cau loto
 */
const statisCauLoto = () => {};

/**
 * Statis cầu bạch thủ
 * @param {*} idprovince
 * @param {*} dateStart
 * @param {*} dateEnd
 * @param {*} amplitude
 */
const statisCauBachThu = async (idProvince, amplitude, dateStart, dateEnd) => {
  // Convert timestamp to date start
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);
  const result = [];

  // Find lotterys by date start to date end and id province
  const resultLotterys = await findLotteryByDateStartToDateEndAndIdProvince(idProvince, dateStart, dateEnd);

  // Replace IdProvince, 1 is North side else south sidereplaceIdRegion
  idProvince = replaceIdRegion(idProvince);

  // Get array last two digits
  const resultArrayNumber = getArrayNumberFromResultLotteryByIdRegionOrIdProvince(resultLotterys, idProvince);

  // Convert mapcount to array object
  const mapCount = countAppearOfNumber(resultArrayNumber);

  mapCount.forEach((value, key) => {
    if (value >= amplitude) {
      result.push(key);
    }
  });

  return result;
};

/**
 * Statis basic array
 * @param {*} idProvince this is idProvince
 * @param {*} type type == 1 thì kiểm tra giải đặt biệt, ngược lại kiểm tra hết tất cả trong lo
 * @param {*} dateStart this is dateStart
 * @param {*} dateEnd this is dateEnd
 */
const statisBasicArrayNumberFrom01To99 = async (idProvince, type, dateStart, dateEnd) => {
  // Convert timestamp to date start
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);
  let resultLotterys = '';
  let resultArrayNumber = '';

  // Find lotterys by date start to date end and id province
  resultLotterys = await findLotteryByDateStartToDateEndAndIdProvince(idProvince, dateStart, dateEnd);

  // Replace IdProvince, 1 is North side else south sidereplaceIdRegion
  idProvince = replaceIdRegion(idProvince);

  // Get Array Result and date appear by id region from Lottery
  resultLotterys = await getArrayResultAndDateAppearByIdRegion(resultLotterys, idProvince);

  // Get array last two digits of special prize and eight prize or seventh prize by idRegion
  // type = 1 check special else check all
  if (type == 1) {
    resultArrayNumber = getArraySpecialPrizeOfResultLotterys(resultLotterys, idProvince);
  } else {
    resultArrayNumber = getArrayNumberFromResultLotteryByIdRegionOrIdProvince(resultLotterys, idProvince);
  }
  // This is map, key is number, value is appear of number. example {3 => 1}
  const mapCount = countAppearOfNumber(resultArrayNumber);

  // Convert mapcount to array object
  resultArrayNumber = getArrayNumberObjectFromMap(mapCount);

  // Add the latest appearance date into result
  resultArrayNumber = addTheLatestAppearanceDateIntoResult(resultArrayNumber, resultLotterys, idProvince, type);

  // Add percent appear into result lottery
  resultArrayNumber = addPercenAppearIntoResultArray(resultArrayNumber);

  // Sort DESC Count appearance of number
  resultArrayNumber = resultArrayNumber.sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0));

  return resultArrayNumber;
};

/**
 * Add percent appear into result lottery
 * @author ThanhVV
 */
const addPercenAppearIntoResultArray = (resultArrayNumber) => {
  let totalValue = 0;

  for (const value of resultArrayNumber) {
    totalValue += value.value;
  }

  // Add percent into result
  for (const value of resultArrayNumber) {
    const original = (value.value / totalValue) * 100;
    value.percent = Math.round(original * 100) / 100;
  }

  return resultArrayNumber;
};

/**
 * Get array special prize of lotterys result
 * @param {*} resultLotterys
 */
const getArraySpecialPrizeOfResultLotterys = (resultLotterys) => {
  const results = [];
  for (const lottery of resultLotterys) {
    // Push special prize
    const arrayOfTwoDigitOfSpecialPrize = getArrayLastTwoDigitOfPrizeNumber(lottery.specialPrize);
    results.push(...arrayOfTwoDigitOfSpecialPrize);
  }
  return results;
};

/**
 *
 * @param {*} type type == 1 is statis of special else statis of loto
 * @param {*} idProvince
 * @param {*} dateStart
 * @param {*} dateEnd
 * @author ThanhVV
 */
const statisHeadAndTailFrom0To9 = async (type, idProvince, dateStart, dateEnd) => {
  // Convert timestamp to date start
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  // Find lottery result
  const lotteryResults = await findLotteryByDateStartToDateEndAndIdProvince(idProvince, dateStart, dateEnd);

  // Save value  of head and tail from lottery result
  const arrayHead = [];
  const arrayTail = [];

  // Object result
  const objectResult = {};

  // If type = 1 check special else check loto
  if (type == 1) {
    lotteryResults.forEach((item) => {
      arrayHead.push(...getHeadFromLotteryResult(item.specialPrize));
      arrayTail.push(...getTailFromLotteryResult(item.specialPrize));
    });
  } else {
    lotteryResults.forEach((item) => {
      arrayHead.push(...getHeadFromLotteryResult(item.specialPrize));
      arrayHead.push(...getHeadFromLotteryResult(item.firstPrize));
      arrayHead.push(...getHeadFromLotteryResult(item.secondPrize));
      arrayHead.push(...getHeadFromLotteryResult(item.thirdPrize));
      arrayHead.push(...getHeadFromLotteryResult(item.fourthPrize));
      arrayHead.push(...getHeadFromLotteryResult(item.fifthPrize));
      arrayHead.push(...getHeadFromLotteryResult(item.sixthPrize));
      arrayHead.push(...getHeadFromLotteryResult(item.seventhPrize));

      if (isTheInputStringValid(item.eighthPrize)) {
        arrayHead.push(...getHeadFromLotteryResult(item.eighthPrize));
      }

      arrayTail.push(...getTailFromLotteryResult(item.specialPrize));
      arrayTail.push(...getTailFromLotteryResult(item.firstPrize));
      arrayTail.push(...getTailFromLotteryResult(item.secondPrize));
      arrayTail.push(...getTailFromLotteryResult(item.thirdPrize));
      arrayTail.push(...getTailFromLotteryResult(item.fourthPrize));
      arrayTail.push(...getTailFromLotteryResult(item.fifthPrize));
      arrayTail.push(...getTailFromLotteryResult(item.sixthPrize));
      arrayTail.push(...getTailFromLotteryResult(item.seventhPrize));

      if (isTheInputStringValid(item.eighthPrize)) {
        arrayTail.push(...getTailFromLotteryResult(item.eighthPrize));
      }
    });
  }

  // This is object result of head or tail, value is the number of occurrences
  const mapHeads = getTheNumberOfOccurrencesOfItemInArray(arrayHead);
  const mapTail = getTheNumberOfOccurrencesOfItemInArray(arrayTail);

  // This is object convert from map and calcuate percent
  const headObject = addPercentIntoObjectFromMap(mapHeads);
  const tailObject = addPercentIntoObjectFromMap(mapTail);

  objectResult.head = headObject;
  objectResult.tail = tailObject;

  return objectResult;
};

/**
 * Get Head From Lottery Result
 * @param {*} lotteryResults
 * @Return head of lottery results example 12345, head is 4
 */
const getHeadFromLotteryResult = (lotteryResult) => {
  const result = [];
  if (isTheInputStringValid(lotteryResult)) {
    lotteryResult = lotteryResult.split(' ');

    lotteryResult.forEach((item) => {
      const { length } = item;
      const value = item.substring(length - 2, length - 1);
      result.push(value);
    });
  }
  return result;
};

/**
 * Get Tail From Lottery Result
 * @param {*} lotteryResult
 * @Return Tail of lottery results example 12345, Tail is 5
 */
const getTailFromLotteryResult = (lotteryResult) => {
  const result = [];
  if (isTheInputStringValid(lotteryResult)) {
    lotteryResult = lotteryResult.split(' ');

    lotteryResult.forEach((item) => {
      const { length } = item;
      const value = item.substring(length - 1, length);
      result.push(value);
    });
  }

  return result;
};

/**
 * Get The Number Of Occurrences Of Item In Array
 * @param {*} arrayResult
 * @return {*} HashMap
 */
const getTheNumberOfOccurrencesOfItemInArray = (arrayResult) => {
  arrayResult.sort();

  const mapCount = new Map();

  arrayResult.forEach((item) => {
    if (mapCount.has(item)) {
      mapCount.set(item, mapCount.get(item) + 1);
    } else {
      mapCount.set(item, 1);
    }
  });

  return mapCount;
};

/**
 * Add percent into object from map
 * @param {*} mapCount
 */
const addPercentIntoObjectFromMap = (mapCount) => {
  const objectResult = [];
  let totalValue = 0;

  mapCount.forEach((values, keys) => {
    totalValue += values;
  });

  mapCount.forEach((values, keys) => {
    const object = {};

    // giá trị này dùng để tính phần trăm của value
    const original = (values / totalValue) * 100;
    object.name = keys;
    object.percent = Math.round(original * 100) / 100;
    object.value = values;

    objectResult.push(object);
  });

  return objectResult;
};

/**
 * Is The Input String Valid
 * @author ThanhVV
 */
const isTheInputStringValid = (stringInput) => {
  if (stringInput == '' || stringInput == null || stringInput == undefined) {
    return false;
  }
  return true;
};

/**
 * Add Nearest return date
 */
const addLastAppearanceDate = (objecResult) => {
  const dateCurrent = new Date();

  objecResult.forEach((item) => {
    const dateAppear = new Date(item.dateAppear);
    const diffTime = Math.abs(dateCurrent - dateAppear);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    item.lastAppearanceDate = diffDays;
  });

  return objecResult;
};

module.exports = {
  createLottery,
  queryLotterys,
  deleteAll,
  findLotterysByIdRegionAndDateStart,
  findLotterysByIdProvince,
  checkLotteryTickets,
  findLotterysByDateStart,
  findLotteryByDateStartToDateEndAndIdRegion,
  findLotteryByDateStartToDateEndAndIdProvince,
  statisThePairsOfNumberThatAppearMost,
  statisAppearOfNumbersWinningFromResult,
  statisLotoGan,
  statisLotoWithTheLastTwoDigits,
  statisBasicArrayNumberFrom01To99,
  statisCauBachThu,
  statisHeadAndTailFrom0To9,
};
