const httpStatus = require('http-status');
const { Menu } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a Menu
 * @param {*} menuBody
 * @returns  {Promise <Menu>}
 */
const createMenu = async (menuBody) => {
  return await Menu.create(menuBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryMenus = async (filter, options) => {
  const menus = await Menu.paginate(filter, options);
  return menus;
};

/**
 * Delete all Data
 * @returns records
 */
const deleteAll = async () => {
  const records = await Menu.deleteMany();
  return records;
};

module.exports = {
  createMenu,
  queryMenus,
  deleteAll,
};
