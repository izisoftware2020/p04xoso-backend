const axios = require('axios');
const cheerio = require('cheerio');
const { Lottery } = require('../models');
const Constant = require('../utils/constant');
const { Power645 } = require('../models');
const { Power655 } = require('../models');
const { Max4D } = require('../models');
const { provinceService } = require('./index');
const { ModelForTest } = require('../models');

/**
 * Insert test
 */
const insertTest = async ()=>{
    const dateCurrent = new Date();
    const gio= dateCurrent.getHours();
    const phut = dateCurrent.getMinutes(); 
    const thoiGian = {'thoiGian':gio+'-'+phut };
    const result =  await ModelForTest.create(thoiGian);
}

/**
 * Get date current
 */
const getDateCurrent = () => {
  // Set date current
  const dateObj = new Date();
  const year = dateObj.getUTCFullYear();
  const month = dateObj.getUTCMonth() + 1; // months from 1-12
  const day = dateObj.getUTCDate();
  const monthParse = month < 10 ? `0${month.toString()}` : month;
  const dateParse = day < 10 ? `0${day.toString()}` : day;

  const dateCurrent = `${year}-${monthParse}-${dateParse}`;

  return dateCurrent;
};

/**
 * Get data crawl from xo so Minh Ngoc
 * @param {*} type 1 is lottery of northside, 2 is centralside, 3 is southside
 * @param {*} liveURL this is live url
 * @author ThanhVV
 */
const getDataCrawlFromWebXoSoMinhNgoc = async (type, liveURL) => {
  // Set object result of northSide
  const objecResultOfNorthSide = {};

  // Set array result of centralSide or north side
  const arrayResult = [];

  // Xu ly ro ri memory
  const source = axios.CancelToken.source();

  // Kiểm tra miền bắc
  if (type == 1) {
    axios(liveURL, {
      CancelToken: source.token,
    })
      .then((response) => response.data)
      .then(async function (response) {
        const html = await response;
        const $ = await cheerio.load(html, { decodeEntities: false });

        objecResultOfNorthSide.specialPrize = $('.giaidb').text();
        objecResultOfNorthSide.firstPrize = $('.giai1').text();
        objecResultOfNorthSide.secondPrize = $('.giai2').text();
        objecResultOfNorthSide.thirdPrize = $('.giai3').text();
        objecResultOfNorthSide.fourthPrize = $('.giai4').text();
        objecResultOfNorthSide.fifthPrize = $('.giai5').text();
        objecResultOfNorthSide.sixthPrize = $('.giai6').text();
        objecResultOfNorthSide.seventhPrize = $('.giai7').text();
        objecResultOfNorthSide.dateStart = getDateFromWebsiteCrawl($('.bkm').text());

        // insert or update lottery of north side
        await insertOrUpdateLotteryOfNorthSide(objecResultOfNorthSide);

        // Cancel request token
        axios.CancelToken;
      })
      .catch(function (err) {
        // Cancel request token
        axios.CancelToken;

        console.log('Server log', err);
      });
  } else {
    axios(liveURL, {
      CancelToken: source.token,
    })
      .then((response) => response.data)
      .then(async function (response) {
        const html = await response;
        const $ = await cheerio.load(html, { decodeEntities: false });

        const tinhs = [];
        $('.tinh').each(function (i, elem) {
          tinhs.push($(this).text().trim());
        });
        arrayResult.push(tinhs);

        const giaiDBs = [];
        $('.giaidb').each(function (i, elem) {
          giaiDBs.push($(this).text().trim());
        });
        giaiDBs.shift();
        arrayResult.push(giaiDBs);

        const giai1s = [];
        $('.giai1').each(function (i, elem) {
          giai1s.push($(this).text().trim());
        });
        giai1s.shift();
        arrayResult.push(giai1s);

        const giai2s = [];
        $('.giai2').each(function (i, elem) {
          giai2s.push($(this).text().trim());
        });
        giai2s.shift();
        arrayResult.push(giai2s);

        const giai3s = [];
        $('.giai3').each(function (i, elem) {
          giai3s.push($(this).text().trim());
        });
        giai3s.shift();
        arrayResult.push(giai3s);

        const giai4s = [];
        $('.giai4').each(function (i, elem) {
          giai4s.push($(this).text().trim());
        });
        giai4s.shift();
        arrayResult.push(giai4s);

        const giai5s = [];
        $('.giai5').each(function (i, elem) {
          giai5s.push($(this).text().trim());
        });
        giai5s.shift();
        arrayResult.push(giai5s);

        const giai6s = [];
        $('.giai6').each(function (i, elem) {
          giai6s.push($(this).text().trim());
        });
        giai6s.shift();
        arrayResult.push(giai6s);

        const giai7s = [];
        $('.giai7').each(function (i, elem) {
          giai7s.push($(this).text().trim());
        });
        giai7s.shift();
        arrayResult.push(giai7s);

        const giai8s = [];
        $('.giai8').each(function (i, elem) {
          giai8s.push($(this).text().trim());
        });
        giai8s.shift();
        arrayResult.push(giai8s);

        // Get date
        arrayResult.push(getDateFromWebsiteCrawl($('.bkm').text()));

        // Insert or update lottery of central or south side
        await insertOrUpdateLotteryOfCentralOrSouthSide(arrayResult, type);

        // Cancel request token
        axios.CancelToken;
      })
      .catch(function (err) {
        // Cancel request token
        axios.CancelToken;

        console.log('Server log', err);
      });
  }
};

/**
 * Get Lotterys from crawl data
 * @param {*} dataWeb
 */
const getLotterysFromCrawlData = (dataWeb) => {
  // Lottery array result
  const lotteryArray = [];

  // Ngày hôm đó xổ mấy đài
  const { length } = dataWeb[0];

  for (let i = 0; i < length; i++) {
    // Values of lotterys
    const valueOfLotterys = [];

    for (let j = 0; j < dataWeb.length; j++) {
      // Value of lottery
      const valueOfLottery = dataWeb[j][i];

      valueOfLotterys.push(valueOfLottery);
    }
    // lottery object
    const lottery = {};

    // Kiểm tra giải 7 có value chưa nếu có rồi thì giải 8 valid
    if (isTheInputStringValid(valueOfLotterys[8])) {
      lottery.eighthPrize = valueOfLotterys[9];
    }

    // Kiểm tra giải 6 có value chưa nếu có rồi thì giải 7 valid
    if (isTheInputStringValid(valueOfLotterys[7])) {
      lottery.seventhPrize = valueOfLotterys[8];
    }

    // Giải 6 xuống phần insert or update xử lý
    lottery.sixthPrize = valueOfLotterys[7];

    // Lưu giải 5 tạm thời
    if(isTheInputStringValid(valueOfLotterys[6])){
      lottery.fifthPrizeTemp = valueOfLotterys[6];
    }

    // Kiểm tra giải 4 có value chưa nếu có rồi thì giải 5 valid
    if (isTheInputStringValid(valueOfLotterys[5])) {
      lottery.fifthPrize = valueOfLotterys[6];
    }

    // Giải 4 xuống phần insert or update xử lý
    lottery.fourthPrize = valueOfLotterys[5];

     // Lưu giải 3 tạm thời
    if(isTheInputStringValid(valueOfLotterys[4])){
      lottery.thirdPrizeTemp = valueOfLotterys[4];
    }

    // Giải 3 xuống phần insert or update xử lý
    lottery.thirdPrize = valueOfLotterys[4];

    // Lưu giải 2 tạm thời
    if(isTheInputStringValid(valueOfLotterys[3])){
      lottery.secondPrizeTemp = valueOfLotterys[3];
    }

    // Kiểm tra giải 1 có value chưa nếu có rồi thì  giải 2 valid
    if (isTheInputStringValid(valueOfLotterys[2])) {
      lottery.secondPrize = valueOfLotterys[3];
    }

    // Kiểm tra giải đặc biệt có value chưa nếu có rồi thì giải 1 valid
    if (isTheInputStringValid(valueOfLotterys[1])) {
      lottery.firstPrize = valueOfLotterys[2];
    }


    // Riêng giải đặt biệt sẽ thay đổi 5s 1 lần
    lottery.specialPrizeTemp = valueOfLotterys[1];
    lottery.province = valueOfLotterys[0];
    lottery.dateStart = dataWeb[10];
    lotteryArray.push(lottery);
  }

  return lotteryArray;
};

/**
 * Is valid six one
 * @param {*} countsixthPrizeOne
 * @param {*} countsixthPrizeTwo
 * @returns
 * @author ThanhVV
 */
const isValidPrizeSixOne = (countsixthPrizeOne, countsixthPrizeTwo) => {
  return (countsixthPrizeOne > 4 && countsixthPrizeTwo < 4) || (countsixthPrizeOne > 4 && !countsixthPrizeTwo);
};

/**
 * Is Valid Prize Six Two
 * @param {*} countsixthPrizeOne
 * @param {*} countsixthPrizeTwo
 * @param {*} countsixthPrizeThree
 * @author ThanhVV
 */
const isValidPrizeSixTwo = (countsixthPrizeOne, countsixthPrizeTwo, countsixthPrizeThree) => {
  return (
    (countsixthPrizeOne > 4 && countsixthPrizeTwo > 4 && countsixthPrizeThree < 4) ||
    (countsixthPrizeOne > 4 && countsixthPrizeTwo > 4 && !countsixthPrizeThree)
  );
};

/**
 * Is valid prize six three
 * @param {*} countsixthPrizeOne
 * @param {*} countsixthPrizeTwo
 * @param {*} countsixthPrizeThree
 * @returns
 * @author ThanhVV
 */
const isValidPrizeSixThree = (countsixthPrizeOne, countsixthPrizeTwo, countsixthPrizeThree) => {
  return countsixthPrizeOne > 4 && countsixthPrizeTwo > 4 && countsixthPrizeThree > 4;
};

/**
 * Is valid prize seventh one
 * @param {*} countSeventhPrizeOne
 * @param {*} countSeventhPrizeTwo
 */
const isValidPrizeSeventhOne = (countSeventhPrizeOne, countSeventhPrizeTwo) => {
  return (countSeventhPrizeOne > 4 && countSeventhPrizeTwo < 4) || (countSeventhPrizeOne > 4 && !countSeventhPrizeTwo);
};

/**
 * Is valid prize seventh two
 * @param {*} countSeventhPrizeTwo
 * @param {*} countSeventhPrizeThree
 * @returns
 */
const isValidPrizeSeventhTwo = (countSeventhPrizeTwo, countSeventhPrizeThree) => {
  return (countSeventhPrizeTwo > 4 && countSeventhPrizeThree < 4) || (countSeventhPrizeTwo > 4 && !countSeventhPrizeThree);
};

/**
 * Is Valid Prize Seventh Three
 * @param {*} countSeventhPrizeThree
 * @param {*} countSeventhPrizeFour
 */
const isValidPrizeSeventhThree = (countSeventhPrizeThree, countSeventhPrizeFour) => {
  return (countSeventhPrizeThree > 4 && countSeventhPrizeFour < 4) || (countSeventhPrizeThree > 4 && !countSeventhPrizeFour);
};

/**
 * Is valid prize seventh four
 */
const isValidPrizeSeventhFour = (countSeventhPrizeFour) => {
  return countSeventhPrizeFour > 4;
};

/**
 * insert or update lottery of north side from live streaming
 * @author ThanhVV
 */
const insertOrUpdateLotteryOfNorthSide = async (dataWeb) => {
  // // Get data web from crawl of xo so minh ngoc
  // const dataWeb = await getDataCrawlFromWebXoSoMinhNgoc(1, Constant.LINK_URL_LIVE_NORTH_SIDE);

  const objectResult = {};
  objectResult.specialPrizeTemp = dataWeb.specialPrize;

  objectResult.dateStart = dataWeb.dateStart;
  objectResult.region = Constant.ID_REGION_NORTH_SIDE;
  objectResult.province = Constant.ID_PROVINCE_NORTH_SIDE;

  // Kiểm tra giải 2 đã có value chưa nếu có rồi thì giải 1 hợp lệ
  if (isTheInputStringValid(dataWeb.secondPrize)) {
    objectResult.firstPrize = dataWeb.firstPrize;
  }

  // Get seconde prize
  objectResult.secondPrize = getResultOfsecondPrizeOfNorthSide(dataWeb.secondPrize, dataWeb.thirdPrize);

  // Get third Prize
  objectResult.thirdPrize = getResultOfThirdPriceOfNorthSide(dataWeb.thirdPrize, dataWeb.fourthPrize);

  // Get fourth prize
  objectResult.fourthPrize = getResultOfFourthPriceOfNorthSide(dataWeb.fourthPrize, dataWeb.fifthPrize);

  // Get fifth prize
  objectResult.fifthPrize = getResultOfFifthPrizeOfNorthSide(dataWeb.fifthPrize, dataWeb.sixthPrize);

  // Get six prize temp
  objectResult.sixthPrizeTemp = dataWeb.sixthPrize;

  // Get seventh prize temp
  objectResult.seventhPrizeTemp = dataWeb.seventhPrize;

  // Get Head and tail from object result
  objectResult.headAndTail = Object.fromEntries(getHeadAndTailFromLotteryResult(objectResult));

  // Find lottery by date start
  const lottery = await Lottery.findOne({ dateStart: objectResult.dateStart, region: Constant.ID_REGION_NORTH_SIDE });

  if (objectResult.sixthPrizeTemp) {
    // Get giai 6
    objectResult.sixthPrizeOneTemp = objectResult.sixthPrizeTemp.substring(0, 3);
    objectResult.sixthPrizeTwoTemp = objectResult.sixthPrizeTemp.substring(3, 6);
    objectResult.sixthPrizeThreeTemp = objectResult.sixthPrizeTemp.substring(6, 9);

    // Get giai 6 1
    if (isTheInputStringValid(lottery.sixthPrizeOneTemp)) {
      // So sanh giai 6-1 tam thoi voi giai 6 - 1 tu crawl
      if (lottery.sixthPrizeOneTemp == objectResult.sixthPrizeOneTemp) {
        if (isTheInputStringValid(lottery.countsixthPrizeOne)) {
          objectResult.countsixthPrizeOne = lottery.countsixthPrizeOne + 1;
        } else {
          objectResult.countsixthPrizeOne = 1;
        }
      } else {
        objectResult.countsixthPrizeOne = 1;
      }
    }

    // Get giai 6 2
    if (isTheInputStringValid(lottery.sixthPrizeTwoTemp)) {
      // So sanh giai 6-2 tam thoi voi giai 6 - 2 tu crawl
      if (lottery.sixthPrizeTwoTemp == objectResult.sixthPrizeTwoTemp) {
        if (isTheInputStringValid(lottery.countsixthPrizeTwo)) {
          objectResult.countsixthPrizeTwo = lottery.countsixthPrizeTwo + 1;
        } else {
          objectResult.countsixthPrizeTwo = 1;
        }
      } else {
        objectResult.countsixthPrizeTwo = 1;
      }
    }

    // Get giai 6 3
    if (isTheInputStringValid(lottery.sixthPrizeThreeTemp)) {
      // So sanh giai 6-3 tam thoi voi giai 6 - 3 tu crawl
      if (lottery.sixthPrizeThreeTemp == objectResult.sixthPrizeThreeTemp) {
        if (isTheInputStringValid(lottery.countsixthPrizeThree)) {
          objectResult.countsixthPrizeThree = lottery.countsixthPrizeThree + 1;
        } else {
          objectResult.countsixthPrizeThree = 1;
        }
      } else {
        objectResult.countsixthPrizeThree = 1;
      }
    }

    // Check count sixthPrize
    if (isValidPrizeSixOne(objectResult.countsixthPrizeOne, objectResult.countsixthPrizeTwo)) {
      objectResult.sixthPrize = objectResult.sixthPrizeOneTemp;
    } else if (
      isValidPrizeSixTwo(objectResult.countsixthPrizeOne, objectResult.countsixthPrizeTwo, objectResult.countsixthPrizeThree)
    ) {
      objectResult.sixthPrize = `${objectResult.sixthPrizeOneTemp} ${objectResult.sixthPrizeTwoTemp}`;
    } else if (
      isValidPrizeSixThree(objectResult.countsixthPrizeOne, objectResult.countsixthPrizeTwo, objectResult.countsixthPrizeThree)
    ) {
      objectResult.sixthPrize = `${objectResult.sixthPrizeOneTemp} ${objectResult.sixthPrizeTwoTemp} ${objectResult.sixthPrizeThreeTemp}`;
    }
  }

  // Get seventh Prize
  if (objectResult.seventhPrizeTemp) {

    objectResult.seventPrizeOneTemp = objectResult.seventhPrizeTemp.substring(0, 2);
    objectResult.seventPrizeTwoTemp = objectResult.seventhPrizeTemp.substring(2, 4);
    objectResult.seventPrizeThreeTemp = objectResult.seventhPrizeTemp.substring(4, 6);
    objectResult.seventPrizeFourTemp = objectResult.seventhPrizeTemp.substring(6, 8);

    // Get giai 7 - 1
    if (isTheInputStringValid(lottery.seventPrizeOneTemp)) {
      // So sanh giai 7-1 tam thoi voi giai 7 - 1 tu crawl
      if (lottery.seventPrizeOneTemp == objectResult.seventPrizeOneTemp) {
        if (isTheInputStringValid(lottery.countSeventhPrizeOne)) {
          objectResult.countSeventhPrizeOne = lottery.countSeventhPrizeOne + 1;
        } else {
          objectResult.countSeventhPrizeOne = 1;
        }
      } else {
        objectResult.countSeventhPrizeOne = 1;
      }
    }

    // Get giai 7 - 2
    if (isTheInputStringValid(lottery.seventPrizeTwoTemp)) {
      // So sanh giai 7-1 tam thoi voi giai 7 - 1 tu crawl
      if (lottery.seventPrizeTwoTemp == objectResult.seventPrizeTwoTemp) {
        if (isTheInputStringValid(lottery.countSeventhPrizeTwo)) {
          objectResult.countSeventhPrizeTwo = lottery.countSeventhPrizeTwo + 1;
        } else {
          objectResult.countSeventhPrizeTwo = 1;
        }
      } else {
        objectResult.countSeventhPrizeTwo = 1;
      }
    }

    // Get giai 7 - 3
    if (isTheInputStringValid(lottery.seventPrizeThreeTemp)) {
      // So sanh giai 7-1 tam thoi voi giai 7 - 1 tu crawl
      if (lottery.seventPrizeThreeTemp == objectResult.seventPrizeThreeTemp) {
        if (isTheInputStringValid(lottery.countSeventhPrizeThree)) {
          objectResult.countSeventhPrizeThree = lottery.countSeventhPrizeThree + 1;
        } else {
          objectResult.countSeventhPrizeThree = 1;
        }
      } else {
        objectResult.countSeventhPrizeThree = 1;
      }
    }

    // Get giai 7 - 4
    if (isTheInputStringValid(lottery.seventPrizeFourTemp)) {
      // So sanh giai 7-1 tam thoi voi giai 7 - 1 tu crawl
      if (lottery.seventPrizeFourTemp == objectResult.seventPrizeFourTemp) {
        if (isTheInputStringValid(lottery.countSeventhPrizeFour)) {
          objectResult.countSeventhPrizeFour = lottery.countSeventhPrizeFour + 1;
        } else {
          objectResult.countSeventhPrizeFour = 1;
        }
      } else {
        objectResult.countSeventhPrizeFour = 1;
      }
    }

    // Check count sevent prize
    if (isValidPrizeSeventhOne(objectResult.countSeventhPrizeOne, objectResult.countSeventhPrizeTwo)) {
      objectResult.seventhPrize = lottery.seventPrizeOneTemp;
    } else if (isValidPrizeSeventhTwo(objectResult.countSeventhPrizeTwo, objectResult.countSeventhPrizeThree)) {
      objectResult.seventhPrize = `${lottery.seventPrizeOneTemp} ${lottery.seventPrizeTwoTemp}`;
    } else if (isValidPrizeSeventhThree(objectResult.countSeventhPrizeThree, objectResult.countSeventhPrizeFour)) {
      objectResult.seventhPrize = `${lottery.seventPrizeOneTemp} ${lottery.seventPrizeTwoTemp} ${lottery.seventPrizeThreeTemp}`;
    } else if (isValidPrizeSeventhFour(lottery.countSeventhPrizeFour)) {
      objectResult.seventhPrize = `${lottery.seventPrizeOneTemp} ${lottery.seventPrizeTwoTemp} ${lottery.seventPrizeThreeTemp} ${lottery.seventPrizeFourTemp}`;

       
    }

    // Kiem tra giai dac biet tam thoi
    if (lottery != null) {
      if (isTheInputStringValid(lottery.specialPrizeTemp)) {
        // So sanh giai dat biet tam thoi voi giai data crawl
        if (lottery.specialPrizeTemp == dataWeb.specialPrize) {
          // Kiem tra xem, da dem bao nhieu lan  tam thoi
          // Neu bang null thi cong   1
          if (isTheInputStringValid(lottery.countTemp)) {
            objectResult.countTemp = lottery.countTemp + 1;
          } else {
            objectResult.countTemp = 1;
          }
        } else {
          objectResult.countTemp = 1;
        }

        // Kiem tra da hon 7 lan, giong nhau giua giai dac biet tam thoi so voi giai dat biet tu website
        if (isTheInputStringValid(lottery.countTemp)) {
          if (lottery.countTemp > 5) {
            objectResult.specialPrize = dataWeb.specialPrize;
          }
        }
      }
    }
  }

  // Check lottery if null created else update
  if (lottery == null) {
    // Check live is happening
    if (isTheInputStringValid(dataWeb.firstPrize)) {
      await Lottery.create(objectResult);
    }
  } else {
    await Lottery.findByIdAndUpdate(lottery._id, objectResult);
  }
};

/**
 * Get result of second price of north side
 * @param {*} secondPrize
 * @author ThanhVV
 */
const getResultOfsecondPrizeOfNorthSide = (secondPrize, thirdPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(secondPrize)) {
    return '';
  }

  const numOne = secondPrize.substring(0, 5);
  const numberTwo = secondPrize.substring(5, 10);

  if (isTheInputStringValid(numberTwo) && !isTheInputStringValid(thirdPrize)) {
    return `${numOne}`;
  }
  if (isTheInputStringValid(thirdPrize)) {
    return `${numOne} ${numberTwo}`;
  }

  return '';
};

/**
 * Get result of third price from north side
 * @param {*} thirdPrizethirdPrize
 * @author ThanhVV
 */
const getResultOfThirdPriceOfNorthSide = (thirdPrize, fourthPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(thirdPrize)) {
    return '';
  }

  const numOne = thirdPrize.substring(0, 5);
  const numTwo = thirdPrize.substring(5, 10);
  const numThree = thirdPrize.substring(10, 15);
  const numFour = thirdPrize.substring(15, 20);
  const numFive = thirdPrize.substring(20, 25);
  const numSix = thirdPrize.substring(25, 30);

  if (isTheInputStringValid(numTwo) && !isTheInputStringValid(numThree)) {
    return `${numOne}`;
  }
  if (isTheInputStringValid(numThree) && !isTheInputStringValid(numFour)) {
    return `${numOne} ${numTwo}`;
  }
  if (isTheInputStringValid(numFour) & !isTheInputStringValid(numFive)) {
    return `${numOne} ${numTwo} ${numThree}`;
  }
  if (isTheInputStringValid(numFive) && !isTheInputStringValid(numSix)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour}`;
  }
  if (isTheInputStringValid(numSix) && !isTheInputStringValid(fourthPrize)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour} ${numFive}`;
  }
  if (isTheInputStringValid(fourthPrize)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour} ${numFive} ${numSix}`;
  }

  return '';
};

/**
 * Get result of fourth price of north side1
 * @param {*} fourthPrize
 * @author  ThanhVV
 */
const getResultOfFourthPriceOfNorthSide = (fourthPrize, fifthPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(fourthPrize)) {
    return '';
  }

  const numOne = fourthPrize.substring(0, 4);
  const numTwo = fourthPrize.substring(4, 8);
  const numThree = fourthPrize.substring(8, 12);
  const numFour = fourthPrize.substring(12, 16);

  if (isTheInputStringValid(numTwo) && !isTheInputStringValid(numThree)) {
    return `${numOne}`;
  }
  if (isTheInputStringValid(numThree) && !isTheInputStringValid(numFour)) {
    return `${numOne} ${numTwo}`;
  }
  if (isTheInputStringValid(numFour) && !isTheInputStringValid(fifthPrize)) {
    return `${numOne} ${numTwo} ${numThree}`;
  }
  if (isTheInputStringValid(fifthPrize)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour}`;
  }

  return '';
};

/**
 * Get result of fifth prize of north side
 * @param {*} fifthPrize
 * @author ThanhVV
 */
const getResultOfFifthPrizeOfNorthSide = (fifthPrize, sixthPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(fifthPrize)) {
    return '';
  }

  const numOne = fifthPrize.substring(0, 4);
  const numTwo = fifthPrize.substring(4, 8);
  const numThree = fifthPrize.substring(8, 12);
  const numFour = fifthPrize.substring(12, 16);
  const numFive = fifthPrize.substring(16, 20);
  const numSix = fifthPrize.substring(20, 24);

  if (isTheInputStringValid(numTwo) && !isTheInputStringValid(numThree)) {
    return `${numOne}`;
  }
  if (isTheInputStringValid(numThree) && !isTheInputStringValid(numFour)) {
    return `${numOne} ${numTwo}`;
  }
  if (isTheInputStringValid(numFour) && !isTheInputStringValid(numFive)) {
    return `${numOne} ${numTwo} ${numThree}`;
  }
  if (isTheInputStringValid(numFive) && !isTheInputStringValid(numSix)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour}`;
  }
  if (isTheInputStringValid(numSix) && !isTheInputStringValid(sixthPrize)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour} ${numFive}`;
  }
  if (isTheInputStringValid(sixthPrize)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour} ${numFive} ${numSix}`;
  }

  return '';
};

/**
 * Get result of sixth prize of north side
 * @param {*} sixthPrize
 * @author ThanhVV
 */
const getResultOfSixthPrizeOfNorthSide = (sixthPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(sixthPrize)) {
    return '';
  }
  const numOne = sixthPrize.substring(0, 3);
  const numTwo = sixthPrize.substring(3, 6);
  const numThree = sixthPrize.substring(6, 9);

  const result = `${numOne} ${numTwo} ${numThree}`;

  return sixthPrize;
};

/**
 * Get Result Of Seventh Prize Of North Side
 * @param {*} seventhPrize
 * @author ThanhVV
 */
const getResultOfSeventhPrizeOfNorthSide = (seventhPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(seventhPrize)) {
    return '';
  }

  const numOne = seventhPrize.substring(0, 2);
  const numTwo = seventhPrize.substring(2, 4);
  const numThree = seventhPrize.substring(4, 6);
  const numFour = seventhPrize.substring(6, 8);

  const result = `${numOne} ${numTwo} ${numThree} ${numFour}`;

  return result;
};

/**
 * Is The Input String Valid
 * @author ThanhVV
 */
const isTheInputStringValid = (stringInput) => {
  return stringInput != undefined && stringInput != null && stringInput != '';
};

/**
 * Insert Or Update Lottery Of Central Or Southside
 * @author ThanhVV
 * @param {*} type 2 is Centralside, 3 Southside
 */
const insertOrUpdateLotteryOfCentralOrSouthSide = async (dataWeb, type) => {
  // Get dataweb from webxoso minh ngoc
  // const dataWeb = await getDataCrawlFromWebXoSoMinhNgoc(type, liveURL);

  // Get lotery from crawl data
  const lotterys = getLotterysFromCrawlData(dataWeb);

  for (const value of lotterys) {
    const province = await provinceService.getProvinceByName(value.province);
    value.province = province._id;
    value.region = type == 2 ? Constant.ID_REGION_CENTRAL_SIDE : Constant.ID_REGION_SOUTH_SIDE;
    value.sixthPrize = getResultOfSixthPrizeOfCentralSideOrNorthSide(value.sixthPrize, value.fifthPrizeTemp);
    value.fourthPrize = getResultOfFourthPriceOfCentralSideOrNorthSide(value.fourthPrize, value.thirdPrizeTemp);
    value.thirdPrize = getResultOfThirdPriceOfCentralSideOrNorthSide(value.thirdPrize, value.secondPrizeTemp);

    console.log(value.fifthPrizeTemp +' - '+value.thirdPrizeTemp+'- '+value.secondPrizeTemp);

    // Get value HeadAndTail from value lottery result
    value.headAndTail = Object.fromEntries(getHeadAndTailFromLotteryResult(value));

    // Find lottery by date start
    const lottery = await Lottery.findOne({ dateStart: value.dateStart, province: province._id });

    // Kiem tra giai dac biet tam thoi
    if (lottery != null) {
      if (isTheInputStringValid(lottery.specialPrizeTemp)) {
        // So sanh giai dat biet tam thoi voi giai data crawl
        if (lottery.specialPrizeTemp == value.specialPrizeTemp) {
          // Kiem tra xem, da dem bao nhieu lan  tam thoi
          // Neu bang null thi cong   1
          if (isTheInputStringValid(lottery.countTemp)) {
            value.countTemp = lottery.countTemp + 1;
          } else {
            value.countTemp = 1;
          }
        } else {
          value.countTemp = 1;
        }

        // Kiem tra da hon 7 lan, giong nhau giua giai dac biet tam thoi so voi giai dat biet tu website
        if (isTheInputStringValid(lottery.countTemp)) {
          if (lottery.countTemp > 5) {
            value.specialPrize = value.specialPrizeTemp;
          }
        }
      }
    }

    // Check lottery if null create else update
    if (lottery == null) {
      // Check live is happening
      if (isTheInputStringValid(dataWeb[9])) {
        await Lottery.create(value);
      }
    } else {
      Object.assign(lottery, value);
      await lottery.save();
    }
  }
};

/**
 * Get Result Of Third Price Of Central Side Or North Side
 * @param {*} thirdPrize
 * @author ThanhVV
 */
const getResultOfThirdPriceOfCentralSideOrNorthSide = (thirdPrize, secondPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(thirdPrize)) {
    return '';
  }

  const numOne = thirdPrize.substring(0, 5);
  const numTwo = thirdPrize.substring(5, 10);

  if (isTheInputStringValid(numTwo) && !isTheInputStringValid(secondPrize)) {
    return `${numOne}`;
  }
  if (isTheInputStringValid(secondPrize)) {
    return `${numOne} ${numTwo}`;
  }

  return '';
};

/**
 * Get Result Of Fourth Price Of Central Side Or NorthSidee
 * @param {*} fourthPrize
 * @author  ThanhVV
 */
const getResultOfFourthPriceOfCentralSideOrNorthSide = (fourthPrize, thirdPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(fourthPrize)) {
    return '';
  }

  const numOne = fourthPrize.substring(0, 5);
  const numTwo = fourthPrize.substring(5, 10);
  const numThree = fourthPrize.substring(10, 15);
  const numFour = fourthPrize.substring(15, 20);
  const numFive = fourthPrize.substring(20, 25);
  const numSix = fourthPrize.substring(25, 30);
  const numSeven = fourthPrize.substring(30, 35);

  // Kiểm tra number two là valid và những giải khác ko valid
  if (isTheInputStringValid(numTwo) && !isTheInputStringValid(numThree)) {
    return numOne;

    // Kiểm tra number three là valid và những giải khác ko valid
  }
  if (isTheInputStringValid(numThree) && !isTheInputStringValid(numFour)) {
    return `${numOne} ${numTwo}`;

    // Kiểm tra number four là valid và những giải khác ko valid
  }
  if (isTheInputStringValid(numFour) && !isTheInputStringValid(numFive)) {
    return `${numOne} ${numTwo} ${numThree}`;

    // Kiểm tra number five là valid và những giải khác ko valid
  }
  if (isTheInputStringValid(numFive) && !isTheInputStringValid(numSix)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour}`;

    // Kiểm tra number six là valid và những giải khác ko valid
  }
  if (isTheInputStringValid(numSix) && !isTheInputStringValid(numSeven)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour} ${numFive}`;

    // Kiểm tra number sevent là valid và thirdPrize không valid
  }
  if (isTheInputStringValid(numSeven) && !isTheInputStringValid(thirdPrize)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour} ${numFive} ${numSix}`;

    // Kiểm tra giải 3 là hợp valid
  }
  if (isTheInputStringValid(thirdPrize)) {
    return `${numOne} ${numTwo} ${numThree} ${numFour} ${numFive} ${numSix} ${numSeven}`;
  }
  return '';
};

/**
 * get Result Of Sixth Prize Of Central Side Or NorthSide
 * @param {*} sixthPrize
 * @author ThanhVV
 */
const getResultOfSixthPrizeOfCentralSideOrNorthSide = (sixthPrize, fifthPrize) => {
  // Check valid result of lottery
  if (!isTheInputStringValid(sixthPrize)) {
    return '';
  }
  const numOne = sixthPrize.substring(0, 4);
  const numTwo = sixthPrize.substring(4, 8);
  const numThree = sixthPrize.substring(8, 12);

  // Kiểm tra number two valid và num three not valid
  if (isTheInputStringValid(numTwo) && !isTheInputStringValid(numThree)) {
    return numOne;

    // Kiểm tra num three valid
  }
  if (isTheInputStringValid(numThree) && !isTheInputStringValid(fifthPrize)) {
    return `${numOne} ${numTwo}`;
    // Kiểm tra fifthPrize valid
  }
  if (isTheInputStringValid(fifthPrize)) {
    return `${numOne} ${numTwo} ${numThree}`;
  }

  return '';
};

/**
 * Get head and tail from lottery result
 * @param {*} lotteryResult
 */
const getHeadAndTailFromLotteryResult = (lotteryResult) => {
  // Map data have key from 0-9, and value is last digit
  const mapResult = new Map();

  for (let i = 0; i < 10; i++) {
    const dataOfLastDigits = [];
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.specialPrize, i));
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.firstPrize, i));
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.secondPrize, i));
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.thirdPrize, i));
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.fourthPrize, i));
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.fifthPrize, i));
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.sixthPrize, i));
    dataOfLastDigits.push(...getLastDigitFromStringData(lotteryResult.seventhPrize, i));

    // Check eightPrize is valid
    if (isTheInputStringValid(lotteryResult.eighthPrize)) {
      dataOfLastDigits.push(getLastDigitFromStringData(lotteryResult.eighthPrize, i));
    }
    mapResult.set(i, dataOfLastDigits.join());
  }
  return mapResult;
};

/**
 * Get last digit from string data
 * @param stringData
 * @param firstDataParam Đây là số đứng gần cuối của string data VD 123456 thì first data là 5
 */
const getLastDigitFromStringData = (stringData, firstDataParam) => {
  // Result data have last data
  const resultData = [];

  if (stringData != null && stringData != undefined) {
    // Convert string to array
    const stringDatas = stringData.split(' ');

    stringDatas.forEach((e) => {
      const { length } = e;
      // Get first data and last data
      const firstData = e.substring(length - 2, length - 1);
      const lastData = e.substring(length - 1, length);

      if (firstData == firstDataParam) {
        resultData.push(lastData);
      }
    });
  }

  return resultData;
};

/**
 * Insert Or Update Power 655
 */
const insertOrUpdatePower655 = async () => {
  axios(Constant.LINK_URL_LIVE_POWER655)
    .then((response) => response.data)
    .then(async function (response) {
      const html = await response;
      const $ = await cheerio.load(html, { decodeEntities: false });

      const power655Result = {};
      power655Result.dateStart = getDateFromDataCrawl($('.ngay').text());
      power655Result.setOfNumberWinningNumber = `${getResultLotteryComputerFromDataCrawl($('.ball_power').text())} ${$(
        '.ball_power2'
      ).text()}`;
      power655Result.jackpot1 = '1 1 1 1 1 1';
      power655Result.jackpot1AmountMatch = $('#slgiaijp1').text().replace(/\t/g, '').replace(/\n/g, '');
      power655Result.jackpot1NumberOfPrizes = $('#giatriJack').text();
      power655Result.jackpot2 = '1 1 1 1 1 0';
      power655Result.jackpot2AmountMatch = $('#slgiaijp2').text();
      power655Result.jackpot2NumberOfPrizes = $('#giatriJack2').text();
      power655Result.jackpot2 = '1 1 1 1 1 0';
      power655Result.jackpot2AmountMatch = $('#slgiaijp2').text();
      power655Result.jackpot2NumberOfPrizes = $('#giatriJack2').text();
      power655Result.firstPrize = '1 1 1 1 1 0';
      power655Result.firstPrizeAmountMatch = $('#slgiai1').text();
      power655Result.firstPrizeNumberOfPrizes = '40.000.000';
      power655Result.secondPrize = '1 1 1 1 0 0';
      power655Result.secondPrizeAmountMatch = $('#slgiai2').text();
      power655Result.secondPrizeNumberOfPrizes = '500.000';
      power655Result.thirdPrize = '1 1 1 0 0 0';
      power655Result.thirdPrizeAmountMatch = $('#slgiai3').text();
      power655Result.thirdPrizeNumberOfPrizes = '50.000';

      let power655 = await Power655.findOne({ dateStart: power655Result.dateStart });

      if (power655 == null) {
        // Check live is happening
        if (isTheInputStringValid(power655Result.setOfNumberWinningNumber)) {
          await Power655.create(power655Result);
        }
      } else {
        power655 = Object.assign(power655, power655Result);
        await power655.save();
      }
    })
    .catch(function (err) {
      console.log('Server log', err);
    });
};

/**
 * Insert or update lottery of max4d
 */
const insertOrUpdatePower645 = async () => {
  axios(Constant.LINK_URL_LIVE_POWER645)
    .then((response) => response.data)
    .then(async function (response) {
      const html = await response;
      const $ = await cheerio.load(html, { decodeEntities: false });

      const power645Result = {};
      power645Result.dateStart = getDateFromDataCrawl($('.ngay').text());
      power645Result.setOfNumberWinningNumber = getResultLotteryComputerFromDataCrawl($('.ball_mega').text());
      power645Result.jackpot = '1 1 1 1 1 1';
      power645Result.jackpotAmountMatch = $('#slgiaijp').text();
      power645Result.jackpotNumberOfPrizes = $('#giatriJack').text();
      power645Result.firstPrize = '1 1 1 1 1 0';
      power645Result.firstPrizeAmountMatch = $('#slgiai1').text();
      power645Result.firstPrizeNumberOfPrizes = '10,000,000';
      power645Result.secondPrize = '1 1 1 1 0 0';
      power645Result.secondPrizeAmountMatch = $('#slgiai2').text();
      power645Result.secondPrizeNumberOfPrizes = '300,000';
      power645Result.thirdPrize = '1 1 1 0 0 0';
      power645Result.thirdPrizeAmountMatch = $('#slgiai3').text();
      power645Result.thirdPrizeNumberOfPrizes = '30,000';

      let power645 = await Power645.findOne({ dateStart: power645Result.dateStart });

      if (power645 == null) {
        // Check live is happening
        if (isTheInputStringValid(power645Result.setOfNumberWinningNumber)) {
          await Power645.create(power645Result);
        }
      } else {
        power645 = Object.assign(power645, power645Result);
        await power645.save();
      }
    })
    .catch(function (err) {
      console.log('Server log', err);
    });
};

/**
 *  Insert or update max 4d
 */
const insertOrUpdateMax4d = async () => {
  axios(Constant.LINK_URL_LIVE_MAX4d)
    .then((response) => response.data)
    .then(async function (response) {
      const html = await response;

      const $ = await cheerio.load(html, { decodeEntities: false });

      const ballFristPrize = $('.ball_giai1').text();
      const ballSecondPrize = $('.ball_giai2').text();
      const ballThirdPrize = $('.ball_giai3').text();

      const max4dResult = {};
      max4dResult.dateStart = getDateFromDataCrawl($('.ngay_max4d').text());
      max4dResult.firstResult = ballFristPrize.substring(0, 4);
      max4dResult.secondResult = `${ballSecondPrize.substring(0, 4)} ${ballSecondPrize.substring(4, 8)}`;
      max4dResult.thirdResult = `${ballThirdPrize.substring(0, 4)} ${ballSecondPrize.substring(
        4,
        8
      )} ${ballSecondPrize.substring(8, 12)}`;
      max4dResult.firstConsolationResult = max4dResult.firstResult.substring(1, 4);
      max4dResult.secondConsolationResult = max4dResult.firstResult.substring(2, 4);

      let max4d = await Max4D.findOne({ dateStart: max4dResult.dateStart });

      if (max4d == null) {
        // Check live is happening
        if (isTheInputStringValid(max4dResult.setOfNumberWinningNumber)) {
          await Max4D.create(max4dResult);
        }
      } else {
        max4d = Object.assign(max4d, max4dResult);
        await max4d.save();
      }
    })
    .catch(function (err) { });
};

/**
 * Get Date From String Data Of Lottery Computer
 */
const getDateFromDataCrawl = (dataCrawl) => {
  const { length } = dataCrawl;
  const dateStart = dataCrawl.substring(length - 10, length);

  return `${dateStart.substring(6, 10)}-${dateStart.substring(3, 5)}-${dateStart.substring(0, 2)}`;
};

/**
 * Get Result Lottery Computer From Data Cawl
 */
const getResultLotteryComputerFromDataCrawl = (dataCrawl) => {
  const resultLotterys = [];
  let ball = '';
  const { length } = dataCrawl;

  // Biến count này dùng để đếm = 2 thì thêm ball vào mảng result
  let count = 0;

  for (let i = 0; i < length; i++) {
    ball += dataCrawl.charAt(i);
    count++;

    if (count == 2) {
      resultLotterys.push(ball);
      count = 0;
      ball = '';
    }
  }
  return resultLotterys.join(' ');
};

/**
 * get Date From Website Crawl
 */
const getDateFromWebsiteCrawl = (dateCrawl) => {
  const { length } = dateCrawl;
  const dateQuery = dateCrawl.substring(length - 23, length - 13).trim();

  const dateConvert = `${dateQuery.substring(6, 10)}-${dateQuery.substring(3, 5)}-${dateQuery.substring(0, 2)}`;

  return dateConvert;
};

module.exports = {
  insertOrUpdateLotteryOfNorthSide,
  insertOrUpdateLotteryOfCentralOrSouthSide,
  insertOrUpdatePower645,
  insertOrUpdatePower655,
  insertOrUpdateMax4d,
  getDataCrawlFromWebXoSoMinhNgoc,
  insertTest,
};
