const httpStatus = require('http-status');
const { Menu2 } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a Menu2
 * @param {*} Menu2Body
 * @returns  {Promise <Menu2>}
 */
const createMenu2 = async (Menu2Body) => {
  return Menu2.create(Menu2Body);
};

/**
 * Delete all Data
 * @returns records
 */
const deleteAll = async () => {
  const records = await Menu2.deleteMany();
  return records;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryMenu2s = async (filter, options) => {
  const Menu2s = await Menu2.paginate(filter, options);
  return Menu2s;
};

module.exports = {
  createMenu2,
  queryMenu2s,
  deleteAll,
};
