const { SoiCau } = require('../models');

/**
 * Find all SoiCau
 * @returns
 */
const find = async () => {
  return SoiCau.find();
};

/**
 * Create a SoiCau
 * @param {*} body
 * @returns
 */
const create = async (body) => {
  return SoiCau.create(body);
};

/**
 * Update a SoiCau
 * @param {*} id
 * @param {*} body
 * @returns
 */
const findByIdAndUpdate = async (id, body) => {
  return SoiCau.findByIdAndUpdate({ _id: id }, body);
};

/**
 * Delete a SoiCau
 * @param {*} ids
 * @returns
 */
const findByIdAndDelete = async (ids) => {
  return SoiCau.deleteMany({ _id: { $in: ids.split(',') } });
};

/**
 * Find a SoiCau by id
 * @param {*} id
 * @returns
 */
const findById = async (id) => {
  return SoiCau.findById({ _id: id });
};

/**
 * Paginate
 * @param {*} filter
 * @param {*} options
 * @returns
 */
const paginate = async (filter, options) => {
  return SoiCau.paginate(filter, options);
};

/**
 * search
 * @param {*} filter
 * @returns
 */
const search = async (filter) => {
  return SoiCau.findOne({
    idLoai: filter.idLoai,
    ngay: { $regex: filter.ngay },
  });
};

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
  search,
};
