const httpStatus = require('http-status');
const { Region } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Find by id
 * @param {*} id this is id of Region
 */
const findByIdRegion = async (id) => {
  return await Region.findById(id);
};

/**
 * Create a Region
 * @param {Object} RegionBody
 * @returns {Promise<Region>}
 */
const createRegion = async (regionBody) => {
  return await Region.create(regionBody);
};

/**
 * Update by id of Region
 * @param {*} regionBody this is body of table Region
 * @returns
 */
const updateByIdRegion = async (id, regionBody) => {
  const region = await findByIdRegion(id);

  Object.assign(region, regionBody);

  await region.save();

  return region;
};

/**
 * DeleteById
 * @param {*} id this is id of table Region
 */
const deleteByIdRegion = async (id) => {
  return await Region.deleteOne({ _id: id });
};

/**
 * Query for Regions
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryRegions = async (filter, options) => {
  const regions = await Region.paginate(filter, options);
  return regions;
};

module.exports = {
  findByIdRegion,
  createRegion,
  updateByIdRegion,
  deleteByIdRegion,
  queryRegions,
};
