const httpStatus = require('http-status');
const { Power655 } = require('../models');
const Power645Serivce = require('./s8-power645.service');
const ApiError = require('../utils/ApiError');

/**
 * Create a Power655
 * @param {*} Power655Body
 * @returns  {Promise <Power655>}
 */
const createPower655 = async (Power655Body) => {
  return await Power655.create(Power655Body);
};

/**
 * Delete all Data
 * @returns records
 */
const deleteAll = async () => {
  const records = await Power655.deleteMany();
  return records;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryPower655s = async (filter, options) => {
  const power655s = await Power655.paginate(filter, options);
  return power655s;
};

/**
 * Find power 645 by date
 * @param {*} dateStart this is date need find
 */
const findPower655sByDateStart = async (dateStart) => {
  dateStart = new Date(dateStart * 1000);

  return await Power655.find({ dateStart });
};

/**
 * Find power 655 by month
 * @param {date} month this is month need query
 * @author ThanhVV
 */
const findPower655sByMonthAndYear = async (month, year) => {
  const result = await Power655.find({
    dateStart: {
      $gte: new Date(year, month - 1, 1),
      $lt: new Date(year, month, 1),
    },
  }).sort({ dateStart: -1 });

  return result;
};

/**
 * find Power645 By Date Start End to Date Pre
 * @param {date} dateStart
 * @param {dateEnd} dateEnd
 * @author ThanhVV
 */
const findPower655ByDateStartToDateEnd = async (dateStart, dateEnd) => {
  const result = await Power655.find({})
    .where('dateStart')
    .gte(dateStart)
    .where('dateStart')
    .lte(dateEnd)
    .sort({ dateStart: -1 });

  return result;
};

/**
 * Find The Pairs Appear The Most
 * @param {*} dateStart
 * @param {*} dateEnd
 * @param {*} ThanhVV
 */
const findThePairsAppearTheMost = async (dateStart, dateEnd) => {
  // Count number appear
  let count = 1;

  // Convert timestamp to date
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  let arrayNumber = [];
  const arrayResult = [];

  // Get result power655s in 1 month
  const result = await findPower655ByDateStartToDateEnd(dateStart, dateEnd);

  // Get set of number
  arrayNumber = Power645Serivce.getArrayNumberFromResult(result);

  arrayNumber.sort();

  // Thêm 1 số này vào data để nó duyệt hết các phần từ array
  arrayNumber.push('99');

  // Count the pairt occurrences from array of number
  for (let i = 1; i < arrayNumber.length; i++) {
    if (arrayNumber[i] == arrayNumber[i - 1]) {
      count++;
    } else {
      const object = {};
      object.name = arrayNumber[i - 1];
      object.value = count;

      count = 1;
      arrayResult.push(object);
    }
  }

  // Sort by property value
  arrayResult.sort((a, b) =>
    a.numOfOccurrences > b.numOfOccurrences ? 1 : b.numOfOccurrences > a.numOfOccurrences ? -1 : 0
  );

  return arrayResult;
};

/**
 * Statistics on the frequency of occurrence of pairs of numbers from 01 to 55 of power655
 * @param {*} dateStart this is date start need query
 * @param {*} dateEnd this is date end need query
 * @author ThanhVV
 */
const countTheFrequencyOfOccurrenceOfPairs = async (dateStart, dateEnd) => {
  const mapCount = new Map();
  let arrayNumberOfLottery = [];
  const arrayResult = [];

  // Replace timestamp to date
  dateStart = new Date(dateStart * 1000);
  dateEnd = new Date(dateEnd * 1000);

  // Get result from DB
  const result = await findPower655ByDateStartToDateEnd(dateStart, dateEnd);

  // Get 01 - 55
  const thirtyFiveNumber = Power645Serivce.getListOfNumber(56);

  // Get array of number from result
  arrayNumberOfLottery = Power645Serivce.getArrayNumberFromResult(result);

  // Set default value
  for (const i of thirtyFiveNumber) {
    mapCount.set(i, 0);
  }

  // Count array number
  for (const i of arrayNumberOfLottery) {
    if (mapCount.has(i)) {
      mapCount.set(i, mapCount.get(i) + 1);
    } else {
      mapCount.set(i, 1);
    }
  }

  // Count the frequency of occurrence of pair
  for (const i of thirtyFiveNumber) {
    const object = {};
    object.name = i;
    object.value = mapCount.get(i);

    arrayResult.push(object);
  }

  return arrayResult;
};

/**
 * check power 645 ticket
 * @param {*} lottery
 * @param {*} dateStart
 * @author ThanhVV
 */
const checkPower655Tickets = async (lotterys, dateStart) => {
  // Object array
  const objectResult = {};

  // Result array
  const resultArray = [];

  // Convert lotterys to array
  lotterys = lotterys.split(',');

  // Get result by date startf
  let resultPower655 = await findPower655sByDateStart(dateStart);

  // Convert ResultPower655 from DB to array of number
  resultPower655 = Power645Serivce.getArrayNumberFromResult(resultPower655);

  // Duyệt hết các phần tử để check từng kết quả
  for (let lottery of lotterys) {
    // Convert lottery to array
    lottery = lottery.split('-');

    // Check valid input
    if (Power645Serivce.isValidArrayNumber(lottery, 2) == false || resultPower655.length != lottery.length + 1) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Result Error || Input not valid');
    }

    // Get result after check
    const resultAfterCheck = checkPower655Ticket(lottery, resultPower655);

    resultArray.push(resultAfterCheck);
  }

  objectResult.result = resultArray;
  objectResult.resultPower655 = resultPower655;

  return objectResult;
};

/**
 *
 * @param {array} lotterys
 * @param {array} results
 * Check Power655 Ticket
 */
const checkPower655Ticket = (lotterys, results) => {
  const conSoDacBiet = results[6];

  // Count the same number
  let count = 0;

  for (let i = 0; i < 6; i++) {
    let flag = false;
    for (let j = 0; j < 6; j++) {
      if (lotterys[i] == results[j]) {
        flag = true;
        break;
      }
    }
    if (flag) {
      count++;
    }
  }
  if (count == 6) {
    return 'jackpot1';
  }
  if (count == 5) {
    let flag = false; // check xem có số nào giống con số thứ 7 ko
    for (const value of lotterys) {
      if (value == conSoDacBiet) {
        flag = true;
      }
    }
    if (flag == true) {
      return 'jackpot2';
    }
    return 'firstPrize';
  }
  if (count == 4) {
    return 'secondPrize';
  }
  if (count == 3) {
    return 'thirdPrize';
  }

  return 'noPrize';
};

module.exports = {
  createPower655,
  queryPower655s,
  deleteAll,
  findPower655sByDateStart,
  findPower655sByMonthAndYear,
  findPower655ByDateStartToDateEnd,
  findThePairsAppearTheMost,
  findThePairsAppearTheMost,
  countTheFrequencyOfOccurrenceOfPairs,
  checkPower655Tickets,
};
