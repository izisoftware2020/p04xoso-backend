const { News } = require('../models');

/**
 * Find all News
 * @returns
 */
const find = async () => {
  return News.find();
};

/**
 * Create a News
 * @param {*} body
 * @returns
 */
const create = async (body) => {
  return News.create(body);
};

/**
 * Update a News
 * @param {*} id
 * @param {*} body
 * @returns
 */
const findByIdAndUpdate = async (id, body) => {
  return News.findByIdAndUpdate({ _id: id }, body);
};

/**
 * Delete a News
 * @param {*} ids
 * @returns
 */
const findByIdAndDelete = async (ids) => {
  return News.deleteMany({ _id: { $in: ids.split(',') } });
};

/**
 * Find a News by id
 * @param {*} id
 * @returns
 */
const findById = async (id) => {
  return News.findById({ _id: id });
};

/**
 * Paginate
 * @param {*} filter
 * @param {*} options
 * @returns
 */
const paginate = async (filter, options) => {
  return News.paginate(filter, options);
};

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
