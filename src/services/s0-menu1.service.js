const httpStatus = require('http-status');
const { Menu1 } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a Menu1
 * @param {*} Menu1Body
 * @returns  {Promise <Menu1>}
 */
const createMenu1 = async (Menu1Body) => {
  return Menu1.create(Menu1Body);
};

/**
 * Delete all Data
 * @returns records
 */
const deleteAll = async () => {
  const records = await Menu1.deleteMany();
  return records;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryMenu1s = async (filter, options) => {
  const menu1s = await Menu1.paginate(filter, options);
  return menu1s;
};

module.exports = {
  createMenu1,
  queryMenu1s,
  deleteAll,
};
