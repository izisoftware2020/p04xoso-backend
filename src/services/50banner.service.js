const { Banner } = require('../models');

/**
 * Find all Banner
 * @returns
 */
const find = async () => {
  return Banner.find();
};

/**
 * Create a Banner
 * @param {*} body
 * @returns
 */
const create = async (body) => {
  return Banner.create(body);
};

/**
 * Update a Banner
 * @param {*} id
 * @param {*} body
 * @returns
 */
const findByIdAndUpdate = async (id, body) => {
  return Banner.findByIdAndUpdate({ _id: id }, body);
};

/**
 * Delete a Banner
 * @param {*} ids
 * @returns
 */
const findByIdAndDelete = async (ids) => {
  return Banner.deleteMany({ _id: { $in: ids.split(',') } });
};

/**
 * Find a Banner by id
 * @param {*} id
 * @returns
 */
const findById = async (id) => {
  return Banner.findById({ _id: id });
};

/**
 * Paginate
 * @param {*} filter
 * @param {*} options
 * @returns
 */
const paginate = async (filter, options) => {
  return Banner.paginate(filter, options);
};

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
