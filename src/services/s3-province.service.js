const httpStatus = require('http-status');
const { createShorthandPropertyAssignment } = require('typescript');
const { Province } = require('../models');

const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createProvince = async (provinceBody) => {
  return Province.create(provinceBody);
};

/**
 * Delete all Data
 * @returns records
 */
const deleteAll = async () => {
  const records = await Province.deleteMany();
  return records;
};

/**
 * Find provinces by id region
 * @param {*} idRegion
 * @author ThanhVV
 */
const findProvincesByIdRegion = async (idRegion) => {
  const result = await Province.find({ region: idRegion }).populate('region');

  return result;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryProvinces = async (filter, options) => {
  const provinces = await Province.paginate(filter, options);
  return provinces;
};

/**
 * Find province by id
 */
const getProvinceById = async (id) => {
  const result = await Province.findById(id);
  if (result == null) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Province not found');
  }

  return result;
};

/**
 * Get Province By Name
 * @param {*} name
 */
const getProvinceByName = async (name) => {
  const result = await Province.findOne({ name });

  if (result == null) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Province not found');
  }

  return result;
};

module.exports = {
  createProvince,
  queryProvinces,
  deleteAll,
  findProvincesByIdRegion,
  getProvinceById,
  getProvinceByName,
};
