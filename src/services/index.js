module.exports.authService = require('./auth.service');
module.exports.emailService = require('./email.service');
module.exports.tokenService = require('./token.service');
module.exports.menu1Service = require('./s0-menu1.service');
module.exports.menu2Service = require('./s0-menu2.service');
module.exports.userService = require('./s0-user.service');
module.exports.menuService = require('./s1-menu.service');
module.exports.regionService = require('./s2-region.service');
module.exports.provinceService = require('./s3-province.service');
module.exports.prizeOpenScheduleService = require('./s4-prize_open_schedule.service');
module.exports.lotteryService = require('./s5-lottery.service');
module.exports.computerLotteryResultService = require('./s6-computer_lottery_result.service');
module.exports.power655Service = require('./s7-power655.service');
module.exports.power645Service = require('./s8-power645.service');
module.exports.max4DService = require('./s9-max4d.service');
module.exports.dreamdecodingService = require('./s10-dream_decoding.service');
module.exports.newsService = require('./s11-news.service');
module.exports.scheduleService = require('./s12-scheduler.service');

// Admin
module.exports.menuService = require('./20menu.service');
module.exports.roleService = require('./30role.service');
module.exports.roleDetailService = require('./40role-detail.service');
module.exports.bannerService = require('./50banner.service');
module.exports.newsService = require('./60news.service');
module.exports.uploadService = require('./999999-upload.service');

module.exports.soiCauService = require('./70soi-cau.service');
module.exports.loaiService = require('./80loai.service');
