// const httpStatus = require('http-status');
const { News } = require('../models');
// const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createNews = async (NewsBody) => {
  return await News.create(NewsBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryNewss = async (filter, options) => {
  const news = await News.paginate(filter, options);
  return news;
};

/**
 * Find by id
 * @param {*} id
 */
const findById = async (id) => {
  const news = await News.findOne({ _id: id });

  return news;
};

module.exports = {
  createNews,
  queryNewss,
  findById,
};
