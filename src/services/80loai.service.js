const { Loai } = require('../models');

/**
 * Find all Loai
 * @returns
 */
const find = async () => {
  return Loai.find();
};

/**
 * Create a Loai
 * @param {*} body
 * @returns
 */
const create = async (body) => {
  return Loai.create(body);
};

/**
 * Update a Loai
 * @param {*} id
 * @param {*} body
 * @returns
 */
const findByIdAndUpdate = async (id, body) => {
  return Loai.findByIdAndUpdate({ _id: id }, body);
};

/**
 * Delete a Loai
 * @param {*} ids
 * @returns
 */
const findByIdAndDelete = async (ids) => {
  return Loai.deleteMany({ _id: { $in: ids.split(',') } });
};

/**
 * Find a Loai by id
 * @param {*} id
 * @returns
 */
const findById = async (id) => {
  return Loai.findById({ _id: id });
};

/**
 * Paginate
 * @param {*} filter
 * @param {*} options
 * @returns
 */
const paginate = async (filter, options) => {
  return Loai.paginate(filter, options);
};

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
