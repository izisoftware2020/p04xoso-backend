const httpStatus = require('http-status');
const escapeStringRegexp = require('escape-string-regexp');
const { DreamDecoding } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createDreamDecoding = async (DreamDecodingBody) => {
  return DreamDecoding.create(DreamDecodingBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryDreamDecodings = async (filter, options) => {
  return await DreamDecoding.paginate(filter, options);
};

/**
 * Find dream decoding by content
 * @param {*} content
 * @author ThanhVV
 */
const findDreamDecodingsByContent = async (content, options) => {
  const filter = { content1: { $regex: new RegExp(content, 'i') } };
  return await DreamDecoding.paginate(filter, options);
};

/**
 * find All Dream Decoding And ReplaceContent
 * @Deprecated
 */
const findAllDreamDecodingAndReplaceContent = async () => {
  const dreamDecondings = await DreamDecoding.find({});

  dreamDecondings.forEach((item) => {
    const dreamDeconding = item;
    dreamDeconding.content1 = changeAccentedCharacterToUnsignedCharacter(dreamDeconding.content);
    dreamDeconding.save();
  });
};

/**
 * Change accented charactr to unsigned charactr
 * @param {*} slug
 * @Deprecated
 */
const changeAccentedCharacterToUnsignedCharacter = (slug) => {
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
  slug = slug.replace(/đ/gi, 'd');
  // Xóa các ký tự đặt biệt
  slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');

  // Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
  // Phòng trường hợp người nhập vào quá nhiều ký tự trắng
  slug = slug.replace(/\-\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-/gi, '-');
  slug = slug.replace(/\-\-/gi, '-');
  // Xóa các ký tự gạch ngang ở đầu và cuối
  slug = `@${slug}@`;
  slug = slug.replace(/\@\-|\-\@|\@/gi, '');

  return slug;
};

module.exports = {
  createDreamDecoding,
  queryDreamDecodings,
  findDreamDecodingsByContent,
  findAllDreamDecodingAndReplaceContent,
};
