const httpStatus = require('http-status');
const { ComputerLotteryResult } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a ComputerLotteryResult
 * @param {*} ComputerLotteryResultBody
 * @returns  {Promise <ComputerLotteryResult>}
 */
const createComputerLotteryResult = async (computerLotteryResultBody) => {
  return await ComputerLotteryResult.create(computerLotteryResultBody);
};

/**
 * Delete all Data
 * @returns records
 */
const deleteAll = async () => {
  const records = await ComputerLotteryResult.deleteMany();
  return records;
};

/**
 * Find Computer Lottery Result By Date Start
 * @param {*} dateStart this is Date Start // current is timestamps
 * @author ThanhVV
 */
const findComputerLotteryResultByDateStart = async (dateStart, limit) => {
  // Params
  const dateEnd = new Date();
  var dateStart = new Date(dateStart * 1000);

  dateEnd.setDate(dateStart.getDate() - limit);

  // Query data
  const result = await ComputerLotteryResult.find({})
    .where('dateStart')
    .gte(dateEnd)
    .where('dateStart')
    .lte(dateStart)
    .sort({ dateStart: -1 });

  return result;
};

/**
 * Query for ComputerLotteryResult
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryComputerLotteryResults = async (filter, options) => {
  const results = await ComputerLotteryResult.paginate(filter, options);
  return results;
};

module.exports = {
  createComputerLotteryResult,
  queryComputerLotteryResults,
  deleteAll,
  findComputerLotteryResultByDateStart,
};
