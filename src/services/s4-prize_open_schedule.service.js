const httpStatus = require('http-status');
const { PrizeOpenSchedule } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createPrizeOpenSchedule = async (PrizeOpenScheduleBody) => {
  return await PrizeOpenSchedule.create(PrizeOpenScheduleBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryPrizeOpenSchedules = async (filter, options) => {
  const PrizeOpenSchedules = await PrizeOpenSchedule.paginate(filter, options);
  return PrizeOpenSchedules;
};

module.exports = {
  createPrizeOpenSchedule,
  queryPrizeOpenSchedules,
};
