const httpStatus = require('http-status');
const { Max4D } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createMax4D = async (Max4DBody) => {
  return Max4D.create(Max4DBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryMax4Ds = async (filter, options) => {
  const max4D = await Max4D.paginate(filter, options);
  return max4D;
};

/**
 * Find max4ds by date start
 * @param {*} dateStart this is date need find
 */
const findMax4dsByDateStart = async (dateStart) => {
  const dateQuery = new Date(dateStart * 1000);

  return await Max4D.find({ dateStart: dateQuery });
};

/**
 * Find max4ds by month and year
 * @param {date} month this is month need query
 * @author ThanhVV
 */
const findMax4dsByMonthAndYear = async (month, year) => {
  const result = await Max4D.find({
    dateStart: {
      $gte: new Date(year, month - 1, 1),
      $lt: new Date(year, month, 1),
    },
  }).sort({ dateStart: 'descending' });

  return result;
};

/**
 * Check Max4d tickets
 * @param {*} lottery this is lotter
 */
const checkMax4dTickets = async (lotterys, dateStart) => {
  // Result array
  const resultArray = [];

  // Convert lotterys to array
  lotterys = lotterys.split(',');

  for (const lottery of lotterys) {
    // This is result max4d from DB
    const resultMax4d = await findMax4dsByDateStart(dateStart);

    // Get map data from result of max4d
    const map = getMapDataFromResultOfMax4d(resultMax4d);

    // Check valid lottery max 4d
    if (!isValidLotteryMax4d(lottery)) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Result Error || Input not valid');
    }
    const objectResult = {};

    // Set value prize number of object
    objectResult.firstResult = getNumberWinFirstResultOfMax4d(lottery, map.get('firstResult'));
    objectResult.secondResult = getNumberWinSecondOrThirdResultOfMax4d(lottery, map.get('secondResult'));
    objectResult.thirdResult = getNumberWinSecondOrThirdResultOfMax4d(lottery, map.get('thirdResult'));
    objectResult.firstConsolationResult = getNumberWinConsolationResult(lottery, map.get('firstConsolationResult'), 1);
    objectResult.secondConsolationResult = getNumberWinConsolationResult(lottery, map.get('secondConsolationResult'), 2);

    resultArray.push(objectResult);
  }

  return resultArray;
};

/**
 * get map data from result of max4d
 * @param {*} resultMax4d this is result of max4d
 */
const getMapDataFromResultOfMax4d = (resultMax4d) => {
  // This is map key is prize and key là result
  const map = new Map();

  // Convert to JSON
  resultMax4d = JSON.parse(JSON.stringify(resultMax4d));

  map.set('firstResult', resultMax4d[0].firstResult);
  map.set('secondResult', resultMax4d[0].secondResult);
  map.set('thirdResult', resultMax4d[0].thirdResult);
  map.set('firstConsolationResult', resultMax4d[0].firstConsolationResult);
  map.set('secondConsolationResult', resultMax4d[0].secondConsolationResult);

  return map;
};

/**
 * Check is valid lottery max4d
 * @param {*} lottery this is lottery number need check
 * @returns
 */
const isValidLotteryMax4d = (lottery) => {
  return lottery.length == 4;
};

/**
 * Check first result
 * @param {*} lottery
 * @param {*} max4dResult
 * @author ThanhVV
 */
const getNumberWinFirstResultOfMax4d = (lottery, max4dResult) => {
  // If win return 1 else return 0
  return lottery == max4dResult ? 1 : 0;
};

/**
 * Check second result of Max4d
 * @param {*} lottery
 * @param {*} max4dResult
 * @author {*} ThanhVV
 */
const getNumberWinSecondOrThirdResultOfMax4d = (lottery, max4dResult) => {
  // Convert String to array
  max4dResult = max4dResult.split(' ');

  // Count number win
  let count = 0;

  for (const result of max4dResult) {
    if (result == lottery) {
      count++;
    }
  }

  return count;
};

/**
 * Check consolation result
 * @param {*} lottery
 */
const getNumberWinConsolationResult = (lottery, max4dResult, type) => {
  const { length } = max4dResult;

  // If type = 1 is check first consolation result
  if (type == 1) {
    const lotteryParams = lottery.substring(1, length + 1);

    if (lotteryParams == max4dResult) {
      return 1;
    }

    // Else is check second consolation result
  } else {
    const lotteryParams = lottery.substring(2, length + 2);

    if (lotteryParams == max4dResult) {
      return 1;
    }
  }

  return 0;
};

module.exports = {
  createMax4D,
  queryMax4Ds,
  findMax4dsByDateStart,
  findMax4dsByMonthAndYear,
  checkMax4dTickets,
};
