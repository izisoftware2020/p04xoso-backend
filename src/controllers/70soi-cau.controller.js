const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { soiCauService } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await soiCauService.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  const soiCau = await soiCauService.create(req.body);
  res.status(httpStatus.CREATED).send(soiCau);
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const soiCau = await soiCauService.findByIdAndUpdate(req.body.id, req.body);
  res.status(httpStatus.CREATED).send(soiCau);
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const soiCau = await soiCauService.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send(soiCau);
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const soiCau = await soiCauService.findById(req.params.id);
  res.status(httpStatus.CREATED).send(soiCau);
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['idLoai']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  options.populate = 'idLoai';

  const result = await soiCauService.paginate(filter, options);
  res.send(result);
});

/**
 * search
 */
const search = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['idLoai', 'ngay']);

  const result = await soiCauService.search(filter);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
  search,
};
