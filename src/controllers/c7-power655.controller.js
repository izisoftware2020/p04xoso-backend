const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { power655Service } = require('../services');

/**
 * Create a Power655s
 */
const createPower655 = catchAsync(async (req, res) => {
  const power655s = await power655Service.createPower655(req.body);
  res.send(power655s);
});

/**
 * Delete all Power655s
 */
const deleteAllPower655s = catchAsync(async (req, res) => {
  const result = await power655Service.deleteAll();
  res.send(result);
});

/**
 * Get list Power655s
 */
const getPower655s = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['dateStart', 'month', 'year']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  if (filter.dateStart != null) {
    // Convert date start
    let { dateStart } = filter;
    dateStart = new Date(dateStart * 1000);

    // Set condition
    filter.dateStart = {
      $lte: dateStart,
    };

    // Kiểm tra tháng và năm
  } else if (filter.month != null && filter.year) {
    const { month } = filter;
    const { year } = filter;

    // Set condition
    filter.dateStart = {
      $gte: new Date(year, month - 1, 1),
      $lte: new Date(year, month, 1),
    };
    // Delete Month and Year
    delete filter.month;
    delete filter.year;
  }
  const result = await power655Service.queryPower655s(filter, options);

  res.send(result);
});

/**
 * find the paris appear the most recent 30 date
 */
const findThePairsAppearTheMost = catchAsync(async (req, res) => {
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await power655Service.findThePairsAppearTheMost(dateStart, dateEnd);

  res.send(result);
});

/**
 * Count the frequency of occurrence of pair
 */
const countTheFrequencyOfOccurrenceOfPairs = catchAsync(async (req, res) => {
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await power655Service.countTheFrequencyOfOccurrenceOfPairs(dateStart, dateEnd);

  res.send(result);
});

/**
 * check Power 655 Tickets
 */
const checkPower655Tickets = catchAsync(async (req, res) => {
  const { lottery } = pick(req.query, ['lottery']);
  const { dateStart } = pick(req.query, ['dateStart']);

  const result = await power655Service.checkPower655Tickets(lottery, dateStart);

  res.send(result);
});

module.exports = {
  createPower655,
  deleteAllPower655s,
  getPower655s,
  findThePairsAppearTheMost,
  countTheFrequencyOfOccurrenceOfPairs,
  checkPower655Tickets,
};
