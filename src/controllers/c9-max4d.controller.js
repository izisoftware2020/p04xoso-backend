const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { max4DService } = require('../services');

/**
 * Create Max4d
 */
const createMax4D = catchAsync(async (req, res) => {
  const max4D = await max4DService.createMax4D(req.body);
  res.send(max4D);
});

/**
 * Get Max4ds
 */
const getMax4Ds = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['dateStart', 'month', 'year']);

  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  if (filter.dateStart != null) {
    // Convert date start
    let { dateStart } = filter;
    dateStart = new Date(dateStart * 1000);

    // Set condition
    filter.dateStart = {
      $lte: dateStart,
    };

    // Kiểm tra tháng và năm
  } else if (filter.month != null && filter.year) {
    const { month } = filter;
    const { year } = filter;

    // Set condition
    filter.dateStart = {
      $gte: new Date(year, month - 1, 1),
      $lte: new Date(year, month, 1),
    };

    // Delete Month and Year
    delete filter.month;
    delete filter.year;
  }
  const result = await max4DService.queryMax4Ds(filter, options);

  res.send(result);
});

/**
 * check Max4d Tickets
 * @param {*} lottery
 * @param {*} dateStart
 */
const checkMax4dTickets = catchAsync(async (req, res) => {
  const { lottery } = pick(req.query, ['lottery']);
  const { dateStart } = pick(req.query, ['dateStart']);

  const result = await max4DService.checkMax4dTickets(lottery, dateStart);

  res.send(result);
});

module.exports = {
  createMax4D,
  getMax4Ds,
  checkMax4dTickets,
};
