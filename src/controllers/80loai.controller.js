const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { loaiService } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await loaiService.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  const loai = await loaiService.create(req.body);
  res.status(httpStatus.CREATED).send(loai);
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const loai = await loaiService.findByIdAndUpdate(req.body.id, req.body);
  res.status(httpStatus.CREATED).send(loai);
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const loai = await loaiService.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send(loai);
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const loai = await loaiService.findById(req.params.id);
  res.status(httpStatus.CREATED).send(loai);
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, []);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  options.populate = '';

  const result = await loaiService.paginate(filter, options);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
