const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { computerLotteryResultService } = require('../services');

/**
 * Create a ComputerLotteryResult
 */
const createComputerLotteryResult = catchAsync(async (req, res) => {
  const computerLotteryResult = await computerLotteryResultService.createComputerLotteryResult(req.body);
  res.send(computerLotteryResult);
});

/**
 * Delete all ComputerLotteryResults
 */
const deleteAllComputerLotteryResults = catchAsync(async (req, res) => {
  const result = await computerLotteryResultService.deleteAll();
  res.send(result);
});

/**
 * Get list ComputerLotteryResult
 */
const getComputerLotteryResults = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['dateStart']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  if (filter.dateStart != null) {
    // Convert date start
    let { dateStart } = filter;
    dateStart = new Date(dateStart * 1000);

    // Set condition
    filter.dateStart = {
      $lte: dateStart,
    };
  }
  const result = await computerLotteryResultService.queryComputerLotteryResults(filter, options);

  res.send(result);
});

module.exports = {
  createComputerLotteryResult,
  deleteAllComputerLotteryResults,
  getComputerLotteryResults,
};
