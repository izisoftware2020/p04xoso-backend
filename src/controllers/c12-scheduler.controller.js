const cron = require('node-cron');
const catchAsync = require('../utils/catchAsync');
const Constant = require('../utils/constant');
const { scheduleService } = require('../services');

// Task live lottery of north side
let taskLiveOfLotteryNorthSide = '';

// Task live lottery of centra side
let taskLiveOfLotteryCentralSide = '';

// Task live lottery of south side
let taskLiveOfLotterySouthSide = '';

// Task live of Power655
let taskLiveOfPower655 = '';

// Task live of Power645
let taskLiveOfPower645 = '';

// Task live of Max4d
let taskLiveOfMax4d = '';

// Task live of TEST
let taskLiveOfTest = '';

/**
 * Start job
 */
const startJob = catchAsync(async () => {
  taskLiveOfLotteryNorthSide.start();
  taskLiveOfLotteryCentralSide.start();
  taskLiveOfLotterySouthSide.start();
  taskLiveOfPower655.start();
  taskLiveOfPower645.start();
  taskLiveOfMax4d.start();
  taskLiveOfTest.start();
});

/**
 * List schedule job
 */
const schedulejob = catchAsync(async (req, res) => {
  // Set time start of lottery north side start time call every five second 18:14 - 18:31
  const timeNorthSide = '*/5 10-40 18 * * *';

  // Set time start of lottery south side start time call every five second 17:14 - 17:31
  const timeCentralSide = '*/5 10-40 17 * * *';

  // Set time start of lottery south side start time call every five second 16:14 - 16:31
  const timeSouthSide = '*/5 10-40 16 * * *';

  // Set time start of power655 south side start time call every five second 18:00 - 18:30
  const timePower655 = '*/5 0-40 18 * * *';

  // Set time start of power645 south side start time call every five second 18:00 - 18:30
  const timePower645 = '*/5 0-40 18 * * *';

  // Set time start of max4d south side start time call every five second 18:00 - 18:30
  const timeMax4d = '*/5 0-40 18 * * *';

  // Đây là dùng để  test vào lúc 16h - 7h = 9h tren sever
  const timeLiveTest = '*/5 0-2 20 * * *'

  // Set scheduler task of lottery north side
  taskLiveOfTest = cron.schedule(
    timeLiveTest,
    async () => {
      console.log('test ', new Date());
      // Kiểm tra đang trong time live
      scheduleService.insertTest();
    },
    {
      scheduled: false,
    }
  );

  // Set scheduler task of lottery north side
  taskLiveOfLotteryNorthSide = cron.schedule(
    timeNorthSide,
    async () => {
      // Kiểm tra đang trong time live
      // 18h10 - 18h40
      console.log('taskLiveOfLotteryNorthSide ', new Date());
      scheduleService.getDataCrawlFromWebXoSoMinhNgoc(1, Constant.LINK_URL_LIVE_NORTH_SIDE);
    },
    {
      scheduled: false,
    }
  );

  // Set scheduler task of central side
  taskLiveOfLotteryCentralSide = cron.schedule(
    timeCentralSide,
    () => {
      // Kiểm tra đang trong time live
      // 17h10 - 17h40
      console.log('taskLiveOfLotteryCentralSide ', new Date());
      scheduleService.getDataCrawlFromWebXoSoMinhNgoc(2, Constant.LINK_URL_LIVE_CENTRAL_SIDE);
    },
    {
      scheduled: false,
    }
  );

  // Set scheduler task of south side
  taskLiveOfLotterySouthSide = cron.schedule(
    timeSouthSide,
    () => {
      // Kiểm tra đang trong time live
      // 16h-10 - 16h40
      console.log('taskLiveOfLotterySouthSide ', new Date());
      scheduleService.getDataCrawlFromWebXoSoMinhNgoc(3, Constant.LINK_URL_LIVE_SOUTH_SIDE);
    },
    {
      scheduled: false,
    }
  );

  // Set scheduler task of Power655
  taskLiveOfPower655 = cron.schedule(
    timePower655,
    () => {
      const currentDate = new Date();
      // Kiểm tra đang trong time live
      // 18h - 18h40
      console.log('taskLiveOfPower655 ', new Date());
      scheduleService.insertOrUpdatePower655();
    },
    {
      scheduled: false,
    }
  );

  // Set scheduler task of Power645
  taskLiveOfPower645 = cron.schedule(
    timePower645,
    () => {
      // Kiểm tra đang trong time live
      // 18h - 18h40
      console.log('taskLiveOfPower645 ', new Date());
      scheduleService.insertOrUpdatePower645();
    },
    {
      scheduled: false,
    }
  );

  // Set scheduler task of Max4d
  taskLiveOfMax4d = cron.schedule(
    timeMax4d,
    () => {
      // Kiểm tra đang trong time live
      // 18h - 18h40
      console.log('taskLiveOfMax4d ', new Date());
      scheduleService.insertOrUpdateMax4d();
    },
    {
      scheduled: false,
    }
  );

  // Run job
  startJob();
});

/**
 * Stop job
 */
const stopJob = catchAsync(async (req, res) => {
  //taskLiveOfLotteryNorthSide.stop();
  //taskLiveOfLotteryCentralSide.stop();
  // taskLiveOfLotterySouthSide.stop();

  return res.send({
    message: 'Job stop',
  });
});

/**
 * Destroy job
 */
const destroyJob = catchAsync(async (req, res) => {
 // taskLiveOfLotteryNorthSide.destroy();
 // taskLiveOfLotteryCentralSide.destroy();
  // taskLiveOfLotterySouthSide.destroy();

  return res.send({ message: 'Job Destroyed' });
});

/**
 * validateCron job
 */
const validateCron = catchAsync(async (req, res) => {
 // taskLiveOfLotteryNorthSide.destroy();
  // taskLiveOfLotteryCentralSide.destroy();
  // taskLiveOfLotteryCentralSide.destroy();

  return res.send({ message: 'validation' });
});

module.exports = {
  schedulejob,
  startJob,
  stopJob,
  destroyJob,
  validateCron,
};
