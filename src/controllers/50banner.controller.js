const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { bannerService } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await bannerService.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  const banner = await bannerService.create(req.body);
  res.status(httpStatus.CREATED).send(banner);
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const banner = await bannerService.findByIdAndUpdate(req.body.id, req.body);
  res.status(httpStatus.CREATED).send(banner);
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const banner = await bannerService.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send(banner);
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const banner = await bannerService.findById(req.params.id);
  res.status(httpStatus.CREATED).send(banner);
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['referclass', 'referclass', 'referclass']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await bannerService.paginate(filter, options);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
