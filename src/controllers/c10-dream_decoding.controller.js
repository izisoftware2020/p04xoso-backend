const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { dreamdecodingService } = require('../services');

/**
 * createDreamDecoding
 */
const createDreamDecoding = catchAsync(async (req, res) => {
  const dreamDecoding = await dreamdecodingService.createDreamDecoding(req.body);
  res.send(dreamDecoding);
});

/**
 * get Dream Decodings
 */
const getDreamDecodings = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await dreamdecodingService.queryDreamDecodings(filter, options);

  res.send(result);
});

/**
 * Find dream decoding by content
 */
const findDreamDecodingsByContent = catchAsync(async (req, res) => {
  const { content } = pick(req.body, ['content']);
  const options = pick(req.body, ['sortBy', 'limit', 'page']);

  const result = await dreamdecodingService.findDreamDecodingsByContent(content, options);

  res.send(result);
});

module.exports = {
  createDreamDecoding,
  getDreamDecodings,
  findDreamDecodingsByContent,
};
