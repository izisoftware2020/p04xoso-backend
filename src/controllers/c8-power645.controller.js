const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { power645Service } = require('../services');

/**
 * create Power 645
 */
const createPower645 = catchAsync(async (req, res) => {
  const power645 = await power645Service.createPower645(req.body);
  res.send(power645);
});

/**
 * get Power645s
 */
const getPower645s = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['dateStart', 'month', 'year']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  if (filter.dateStart != null) {
    // Convert date start
    let { dateStart } = filter;
    dateStart = new Date(dateStart * 1000);

    // Set condition
    filter.dateStart = {
      $lte: dateStart,
    };

    // Kiểm tra tháng và năm
  } else if (filter.month != null && filter.year) {
    const { month } = filter;
    const { year } = filter;

    // Set condition
    filter.dateStart = {
      $gte: new Date(year, month - 1, 1),
      $lte: new Date(year, month, 1),
    };

    // Delete Month and Year
    delete filter.month;
    delete filter.year;
  }

  const result = await power645Service.queryPower645s(filter, options);
  res.send(result);
});

/**
 * find Power 645s By DateStart
 */
const findPower645sByMonthAndYear = catchAsync(async (req, res) => {
  const { month } = pick(req.query, ['month']);
  const { year } = pick(req.query, ['year']);

  const result = await power645Service.findPower645sByMonthAndYear(month, year);

  res.send(result);
});

/**
 * find the paris appear the most recent 30 date
 */
const findThePairsAppearTheMost = catchAsync(async (req, res) => {
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await power645Service.findThePairsAppearTheMost(dateStart, dateEnd);

  res.send(result);
});

/**
 * count the frequency of occurrence of pair
 */
const countTheFrequencyOfOccurrenceOfPairs = catchAsync(async (req, res) => {
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await power645Service.countTheFrequencyOfOccurrenceOfPairs(dateStart, dateEnd);

  res.send(result);
});

/**
 * check Power 645 Tickets
 */
const checkPower645Tickets = catchAsync(async (req, res) => {
  const { lottery } = pick(req.query, ['lottery']);
  const { dateStart } = pick(req.query, ['dateStart']);

  const result = await power645Service.checkPower645Tickets(lottery, dateStart);

  res.send(result);
});

module.exports = {
  createPower645,
  getPower645s,
  findPower645sByMonthAndYear,
  findThePairsAppearTheMost,
  countTheFrequencyOfOccurrenceOfPairs,
  checkPower645Tickets,
};
