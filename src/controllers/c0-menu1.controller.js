const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { menu1Service } = require('../services');

/**
 * Create a menu
 */
const createMenu1 = catchAsync(async (req, res) => {
  const menu1 = await menu1Service.createMenu1(req.body);
  res.send(menu1);
});

/**
 * Delete all menus
 */
const deleteAllMenu1s = catchAsync(async (req, res) => {
  const result = await menu1Service.deleteAll();
  res.send(result);
});

/**
 * Get list menu
 */
const getMenu1s = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  options.populate = 'menu2';
  const result = await menu1Service.queryMenu1s(filter, options);

  res.send(result);
});

module.exports = {
  createMenu1,
  deleteAllMenu1s,
  getMenu1s,
};
