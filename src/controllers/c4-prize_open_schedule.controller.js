const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { prizeOpenScheduleService } = require('../services');

/**
 * Create prize open schedule
 */
const createPrizeOpenSchedule = catchAsync(async (req, res) => {
  const prizeOpenSchedule = await prizeOpenScheduleService.createPrizeOpenSchedule(req.body);
  res.send(prizeOpenSchedule);
});

/**
 * Get Prize open schedules
 */
const getPrizeOpenSchedules = catchAsync(async (req, res) => {
  const filter = pick(req.query, []);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  options.populate = 'region';

  const result = await prizeOpenScheduleService.queryPrizeOpenSchedules(filter, options);
  res.send(result);
});

module.exports = {
  createPrizeOpenSchedule,
  getPrizeOpenSchedules,
};
