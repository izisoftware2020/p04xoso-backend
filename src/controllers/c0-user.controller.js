const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await userService.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  req.body.password = 'password';
  const account = await userService.create(req.body);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const account = await userService.findByIdAndUpdate(req.body.id, req.body);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const account = await userService.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const account = await userService.findById(req.params.id);
  res.status(httpStatus.CREATED).send(account);
});

/**
 * Get Permission Of Component
 */
const getPermissionOfComponent = catchAsync(async (req, res) => {
  const { userId } = req.body;
  const { slug } = req.body;

  const data = await userService.getPermissionOfComponent(userId, slug);

  res.send(data);
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['idUser', 'idRole']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await userService.paginate(filter, options);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
  getPermissionOfComponent,
  paginate,
};
