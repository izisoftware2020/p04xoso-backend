const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { provinceService } = require('../services');

/**
 * Create provinces
 */
const createProvince = catchAsync(async (req, res) => {
  const province = await provinceService.createProvince(req.body);
  res.send(province);
});

/**
 * Delete all menus
 */
const deleteAllProvinces = catchAsync(async (req, res) => {
  const result = await provinceService.deleteAll();
  res.send(result);
});

/**
 * Get Profinces
 */
const getProvinces = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['region', 'symbol']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  options.populate = 'region';

  const result = await provinceService.queryProvinces(filter, options);

  res.send(result);
});

module.exports = {
  createProvince,
  getProvinces,
  deleteAllProvinces,
};
