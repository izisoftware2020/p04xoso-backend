module.exports.authController = require('./auth.controller');
module.exports.menu1Controller = require('./c0-menu1.controller');
module.exports.menu2Controller = require('./c0-menu1.controller');
module.exports.userController = require('./c0-user.controller');
module.exports.regionController = require('./c2-region.controller');
module.exports.provinceController = require('./c3-province.controller');
module.exports.prizeOpenScheduleController = require('./c4-prize_open_schedule.controller');
module.exports.lotteryController = require('./c5-lottery.controller');
module.exports.computerLotteryController = require('./c6-computer_lottery_result.controller');
module.exports.power655Controller = require('./c7-power655.controller');
module.exports.power645Controller = require('./c8-power645.controller');
module.exports.max4DController = require('./c9-max4d.controller');
module.exports.dreamDecodingController = require('./c10-dream_decoding.controller');
module.exports.newsController = require('./c11-news.controller');

// Admin
module.exports.menuControler = require('./20menu.controller');
module.exports.roleControler = require('./30role.controller');
module.exports.roleDetailControler = require('./40role-detail.controller');
module.exports.bannerControler = require('./50banner.controller');
module.exports.newsControler = require('./60news.controller');

module.exports.uploadController = require('./999999-upload.controller');

module.exports.soiCauController = require('./70soi-cau.controller');
module.exports.loaiController = require('./80loai.controller');
