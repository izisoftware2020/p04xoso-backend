const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const Constant = require('../utils/constant');
const { lotteryService } = require('../services');

/**
 * Create a Lottery
 */
const createLottery = catchAsync(async (req, res) => {
  const lottery = await lotteryService.createLottery(req.body);
  res.send(lottery);
});

/**
 * Delete all Lotterys
 */
const deleteAllLotterys = catchAsync(async (req, res) => {
  const result = await lotteryService.deleteAll();
  res.send(result);
});

/**
 * Get list Lottery
 */
const getLotterys = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['region', 'province', 'dateStart', 'dateEnd']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  options.populate = 'region, province';

  // Add condition into filter
  if (filter.region != null && filter.dateStart != null) {
    // Convert dateStart
    let { dateStart } = filter;
    dateStart = new Date(dateStart * 1000);

    // Set condition
    filter.dateStart = { $lte: dateStart };
  } else if (filter.province != null && filter.dateStart != null && filter.dateEnd != null) {
    // Convert dateStart and dateEnd
    let { dateStart } = filter;
    let { dateEnd } = filter;
    dateStart = new Date(dateStart * 1000);
    dateEnd = new Date(dateEnd * 1000);

    // Set condition
    filter.dateStart = {
      $gte: dateStart,
      $lte: dateEnd,
    };

    // Delete filter dateStart and filter dateEnd
    delete filter.dateEnd;
  } else if (filter.province != null && filter.dateStart) {
    // Convert dateStart
    let { dateStart } = filter;
    dateStart = new Date(dateStart * 1000);

    // Set condition
    filter.dateStart = {
      $lte: dateStart,
    };
  } else if (filter.dateStart != null) {
    // Convert dateStart
    let { dateStart } = filter;
    dateStart = new Date(dateStart * 1000);

    // Set condition
    filter.dateStart = dateStart;
  }

  const result = await lotteryService.queryLotterys(filter, options);

  res.send(result);
});

/**
 * Check lottery tickets by Date Start, IdProvince And Lottery
 * @param {*} dateStart this is date need query
 * @param {*} idProvince this is province need query
 * @param {*} lottery this is lottery need query
 * @autho ThanhVV
 *
 */
const checkLotteryTickets = catchAsync(async (req, res) => {
  const { lottery } = pick(req.query, ['lottery']);
  const { dateStart } = pick(req.query, ['dateStart']);
  const { idProvince } = pick(req.query, ['idProvince']);

  const result = await lotteryService.checkLotteryTickets(dateStart, idProvince, lottery);

  res.send(result);
});

/**
 * Count the pairs of number that appear most
 */
const statisThePairsOfNumberThatAppearMost = catchAsync(async (req, res) => {
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);
  const { idRegion } = pick(req.query, ['idRegion']);
  const { idProvince } = pick(req.query, ['idProvince']);
  let result = '';

  if (idRegion != null) {
    result = await lotteryService.statisThePairsOfNumberThatAppearMost(idRegion, dateStart, dateEnd, 1);
  } else if (idProvince != null) {
    result = await lotteryService.statisThePairsOfNumberThatAppearMost(idProvince, dateStart, dateEnd, 2);
  }

  res.send(result);
});

/**
 * Statis appear of numbers wining from result
 */
const statisAppearOfNumbersWinningFromResult = catchAsync(async (req, res) => {
  const { lottery } = pick(req.query, ['lottery']);
  const { idProvince } = pick(req.query, ['idProvince']);
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await lotteryService.statisAppearOfNumbersWinningFromResult(lottery, idProvince, dateStart, dateEnd);

  res.send(result);
});

/**
 * Thống kê loto gan
 */
const statisLotoGan = catchAsync(async (req, res) => {
  const { type } = pick(req.query, ['type']);
  const { lottery } = pick(req.query, ['lottery']);
  const { idProvince } = pick(req.query, ['idProvince']);
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await lotteryService.statisLotoGan(type, idProvince, lottery, dateStart, dateEnd);

  res.send(result);
});

/**
 * Thống kê loto với hai số cuối
 */
const statisLotoWithTheLastTwoDigits = catchAsync(async (req, res) => {
  const { idProvince } = pick(req.query, ['idProvince']);
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await lotteryService.statisLotoWithTheLastTwoDigits(idProvince, dateStart, dateEnd);

  res.send(result);
});

/**
 * Thống kê cơ bản các cập số từ 01-99
 */
const statisBasicArrayNumberFrom01To99 = catchAsync(async (req, res) => {
  const { idProvince } = pick(req.query, ['idProvince']);
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);
  const { type } = pick(req.query, ['type']);

  const result = await lotteryService.statisBasicArrayNumberFrom01To99(idProvince, type, dateStart, dateEnd);

  res.send(result);
});

/**
 * Thống kê cầu bạch thủ
 */
const statisCauBachThu = catchAsync(async (req, res) => {
  const { idProvince } = pick(req.query, ['idProvince']);
  const { dateStart } = pick(req.query, ['dateStart']);
  const { amplitude } = pick(req.query, ['amplitude']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await lotteryService.statisCauBachThu(idProvince, amplitude, dateStart, dateEnd);

  res.send(result);
});

/**
 * Thống kê đầu và đuôi tù 0 - 9
 */
const statisHeadAndTailFrom0To9 = catchAsync(async (req, res) => {
  const { type } = pick(req.query, ['type']);
  const { idProvince } = pick(req.query, ['idProvince']);
  const { dateStart } = pick(req.query, ['dateStart']);
  const { dateEnd } = pick(req.query, ['dateEnd']);

  const result = await lotteryService.statisHeadAndTailFrom0To9(type, idProvince, dateStart, dateEnd);

  res.send(result);
});

module.exports = {
  createLottery,
  deleteAllLotterys,
  getLotterys,
  checkLotteryTickets,
  statisThePairsOfNumberThatAppearMost,
  statisAppearOfNumbersWinningFromResult,
  statisLotoGan,
  statisLotoWithTheLastTwoDigits,
  statisBasicArrayNumberFrom01To99,
  statisCauBachThu,
  statisHeadAndTailFrom0To9,
};
