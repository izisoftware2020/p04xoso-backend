const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { newsService } = require('../services');

/**
 * find
 */
const find = catchAsync(async (req, res) => {
  const result = await newsService.find();
  res.send(result);
});

/**
 * create
 */
const create = catchAsync(async (req, res) => {
  const news = await newsService.create(req.body);
  res.status(httpStatus.CREATED).send(news);
});

/**
 * find By Id And Update
 */
const findByIdAndUpdate = catchAsync(async (req, res) => {
  const news = await newsService.findByIdAndUpdate(req.body.id, req.body);
  res.status(httpStatus.CREATED).send(news);
});

/**
 * find By Id And Delete
 */
const findByIdAndDelete = catchAsync(async (req, res) => {
  const news = await newsService.findByIdAndDelete(req.params.id);
  res.status(httpStatus.CREATED).send(news);
});

/**
 * findById
 */
const findById = catchAsync(async (req, res) => {
  const news = await newsService.findById(req.params.id);
  res.status(httpStatus.CREATED).send(news);
});

/**
 * paginate
 */
const paginate = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['referclass', 'referclass', 'referclass']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await newsService.paginate(filter, options);
  res.send(result);
});

module.exports = {
  find,
  create,
  findByIdAndUpdate,
  findByIdAndDelete,
  findById,
  paginate,
};
