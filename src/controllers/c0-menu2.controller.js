const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { menu2Service } = require('../services');

/**
 * Create a menu
 */
const createMenu2 = catchAsync(async (req, res) => {
  const menu2 = await menu2Service.createMenu2(req.body);
  res.send(menu2);
});

/**
 * Delete all menus
 */
const deleteAllMenu2s = catchAsync(async (req, res) => {
  const result = await menu2Service.deleteAll();
  res.send(result);
});

/**
 * Get list menu
 */
const getMenu2s = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await menu2Service.queryMenu2s(filter, options);
  res.send(result);
});

module.exports = {
  createMenu2,
  deleteAllMenu2s,
  getMenu2s,
};
