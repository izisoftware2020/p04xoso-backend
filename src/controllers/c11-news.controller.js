const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { newsService } = require('../services');
const { dreamdecodingService } = require('../services');

/**
 * Create news
 */
const createNews = catchAsync(async (req, res) => {
  const news = await newsService.createNews(req.body);

  res.send(news);
});

/**
 * Get news
 */
const getNews = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await newsService.queryNewss(filter, options);

  res.send(result);
});

/**
 * Find by id
 */
const findById = catchAsync(async (req, res) => {
  const { id } = req.params;

  const result = await newsService.findById(id);

  res.send(result);
});

module.exports = {
  createNews,
  getNews,
  findById,
};
