const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { regionService } = require('../services');

/**
 * Find by id
 * @param {*} id this is id of Region
 */
const findByIdRegion = catchAsync(async (req, res) => {
  const region = await regionService.findByIdRegion(req.params.id);

  res.send(region);
});

/**
 * Create Region
 */
const createRegion = catchAsync(async (req, res) => {
  const region = await regionService.createRegion(req.body);
  res.send(region);
});

/**
 * Update data by id
 */
const updateByIdRegion = catchAsync(async (req, res) => {
  const region = await regionService.updateByIdRegion(req.body.id, req.body);
  res.send(region);
});

/**
 * Delete data by Id
 */
const deleteByIdRegion = catchAsync(async (req, res) => {
  await regionService.deleteByIdRegion(req.params.id);
  res.send({});
});

/**
 * Get Regions
 */
const getRegions = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await regionService.queryRegions(filter, options);

  res.send(result);
});

module.exports = {
  findByIdRegion,
  createRegion,
  updateByIdRegion,
  deleteByIdRegion,
  getRegions,
};
