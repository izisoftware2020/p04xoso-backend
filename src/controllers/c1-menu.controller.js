const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { menuService } = require('../services');

/**
 * Create a menu
 */
const createMenu = catchAsync(async (req, res) => {
  const menu = await menuService.createMenu(req.body);
  res.send(menu);
});

/**
 * Delete all menus
 */
const deleteAllMenus = catchAsync(async (req, res) => {
  const result = await menuService.deleteAll();
  res.send(result);
});

/**
 * Get list menu
 */
const getMenus = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  options.populate = 'menu';

  const result = await menuService.queryMenus(filter, options);

  res.send(result);
});

module.exports = {
  createMenu,
  deleteAllMenus,
  getMenus,
};
