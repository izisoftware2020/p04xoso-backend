const mongoose = require('mongoose');
const schedule = require('node-schedule');
const app = require('./app');
const config = require('./config/config');
const logger = require('./config/logger');
const schedulerController = require('./controllers/c12-scheduler.controller');

let server;
mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
  logger.info('Connected to MongoDB');

  server = app.listen(config.port, () => {
    // Call scheduler
    logger.info('Call scheduler job');
    schedulerController.schedulejob();
    logger.info(`Listening to port ${config.port}`);
  });
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});
