const mongoose = require('mongoose');

const { toJSON, paginate } = require('./plugins');

const loaiSchema = mongoose.Schema(
  {
    name: { type: String, trim: true },
    key: { type: Number, trim: true },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
loaiSchema.plugin(toJSON);
loaiSchema.plugin(paginate);

/**
 * @typedef Loai
 */
const Loai = mongoose.model('Loai', loaiSchema);

module.exports = Loai;
