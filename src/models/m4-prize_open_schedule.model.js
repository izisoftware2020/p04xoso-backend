const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const prizeOpenScheduleSchema = mongoose.Schema(
  {
    startTime: {
      type: Date,
    },
    monday: {
      type: String,
    },
    tuesday: {
      type: String,
    },
    wednesday: {
      type: String,
    },
    thursday: {
      type: String,
    },
    friday: {
      type: String,
    },
    saturday: {
      type: String,
    },
    saturday: {
      type: String,
    },
    sunday: {
      type: String,
    },
    region: {
      type: Schema.Types.ObjectId,
      ref: 'Region',
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
prizeOpenScheduleSchema.plugin(toJSON);
prizeOpenScheduleSchema.plugin(paginate);

/**
 * @typedef Province
 */
const PrizeOpenSchedule = mongoose.model('PrizeOpenSchedule', prizeOpenScheduleSchema);

module.exports = PrizeOpenSchedule;
