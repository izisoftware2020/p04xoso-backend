const mongoose = require('mongoose');

const { toJSON, paginate } = require('./plugins');

const ModelForTestSchema = mongoose.Schema(
  {
    thoiGian: { type: String, trim: true },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
ModelForTestSchema.plugin(toJSON);
ModelForTestSchema.plugin(paginate);

/**
 * @typedef Loai
 */
const ModelForTest = mongoose.model('ModelForTest', ModelForTestSchema);

module.exports = ModelForTest;
