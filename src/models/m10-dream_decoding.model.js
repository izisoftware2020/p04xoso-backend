const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const dreamdecodingSchema = mongoose.Schema(
  {
    content: {
      type: String,
    },
    content1: {
      type: String,
    },
    correspondingSetOfNumbers: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
dreamdecodingSchema.plugin(toJSON);
dreamdecodingSchema.plugin(paginate);

/**
 * @typedef DreamDecoding
 */
const DreamDecoding = mongoose.model('dreamdecoding', dreamdecodingSchema);

module.exports = DreamDecoding;
