const mongoose = require('mongoose');

const { toJSON, paginate } = require('./plugins');

const soiCauSchema = mongoose.Schema(
  {
    idLoai: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Loai',
      trim: true,
    },
    ngay: { type: String, trim: true },
    noiDung: { type: String, trim: true },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  }
);

// add plugin that converts mongoose to json
soiCauSchema.plugin(toJSON);
soiCauSchema.plugin(paginate);

/**
 * @typedef SoiCau
 */
const SoiCau = mongoose.model('SoiCau', soiCauSchema);

module.exports = SoiCau;
