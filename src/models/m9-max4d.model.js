const mongoose = require('mongoose');
// const Schema = mongoose.Schema;
const { toJSON, paginate } = require('./plugins');

const max4DSchema = mongoose.Schema(
  {
    dateStart: {
      type: Date,
    },
    firstResult: {
      type: String,
    },
    secondResult: {
      type: String,
      required: true,
    },
    thirdResult: {
      type: String,
      required: true,
    },
    firstConsolationResult: {
      type: String,
      required: true,
    },
    secondConsolationResult: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
max4DSchema.plugin(toJSON);
max4DSchema.plugin(paginate);

/**
 * @typedef Max4D
 */
const Max4D = mongoose.model('max4D', max4DSchema);

module.exports = Max4D;
