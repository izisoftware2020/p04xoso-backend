const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const provinceSchema = mongoose.Schema(
  {
    symbol: {
      type: String,
    },
    name: {
      type: String,
    },
    region: {
      type: Schema.Types.ObjectId,
      ref: 'Region',
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
provinceSchema.plugin(toJSON);
provinceSchema.plugin(paginate);

/**
 * @typedef Province
 */
const Province = mongoose.model('Province', provinceSchema);

module.exports = Province;
