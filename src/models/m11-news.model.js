const mongoose = require('mongoose');

const { Schema } = mongoose;
const { toJSON, paginate } = require('./plugins');

const newsSchema = mongoose.Schema(
  {
    idMenu: {
      type: Schema.Types.ObjectId,
      ref: 'Menu',
    },
    title: {
      type: String,
    },
    dateCreated: {
      type: Date,
    },
    mainImage: {
      type: String,
    },
    body: {
      type: String,
    },
    tags: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
newsSchema.plugin(toJSON);
newsSchema.plugin(paginate);

/**
 * @typedef News
 */
const News = mongoose.model('news', newsSchema);

module.exports = News;
