const mongoose = require('mongoose');

// const { Schema } = mongoose;
const { toJSON, paginate } = require('./plugins');

const power645Schema = mongoose.Schema(
  {
    dateStart: {
      type: Date,
    },
    setOfNumberWinningNumber: {
      type: String,
    },
    jackpot: {
      type: String,
      required: true,
    },
    jackpotAmountMatch: {
      type: String,
      required: true,
    },
    jackpotNumberOfPrizes: {
      type: String,
      required: true,
    },
    firstPrize: {
      type: String,
      required: true,
    },
    firstPrizeAmountMatch: {
      type: String,
      required: true,
    },
    firstPrizeNumberOfPrizes: {
      type: String,
      required: true,
    },
    secondPrize: {
      type: String,
      required: true,
    },
    secondPrizeAmountMatch: {
      type: String,
      required: true,
    },
    secondPrizeNumberOfPrizes: {
      type: String,
      required: true,
    },
    thirdPrize: {
      type: String,
      required: true,
    },
    thirdPrizeAmountMatch: {
      type: String,
      required: true,
    },
    thirdPrizeNumberOfPrizes: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
power645Schema.plugin(toJSON);
power645Schema.plugin(paginate);

/**
 * @typedef Power645
 */
const Power645 = mongoose.model('Mega645', power645Schema);

module.exports = Power645;
