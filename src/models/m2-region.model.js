const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const regionSchema = mongoose.Schema(
  {
    name: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
regionSchema.plugin(toJSON);
regionSchema.plugin(paginate);

/**
 * @typedef Region
 */
const Region = mongoose.model('Region', regionSchema);

module.exports = Region;
