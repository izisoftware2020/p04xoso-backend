const mongoose = require('mongoose');

const { Schema } = mongoose;
const { toJSON, paginate } = require('./plugins');

const roleDetailSchema = mongoose.Schema(
  {
    role: {
      type: Schema.Types.ObjectId,
      ref: 'Role',
      trim: true,
    },
    menu: {
      type: Schema.Types.ObjectId,
      ref: 'Menu',
      trim: true,
    },
    status: {
      type: String,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
roleDetailSchema.plugin(toJSON);
roleDetailSchema.plugin(paginate);

/**
 * @typedef RoleDetail
 */
const RoleDetail = mongoose.model('RoleDetail', roleDetailSchema);

module.exports = RoleDetail;
