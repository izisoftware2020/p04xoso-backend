const mongoose = require('mongoose');

const { toJSON, paginate } = require('./plugins');

const newsSchema = mongoose.Schema(
  {
    title: { type: String, trim: true },
    body: { type: String, trim: true },
    mainImage: { type: String, trim: true },
    tags: { type: String, trim: true },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
newsSchema.plugin(toJSON);
newsSchema.plugin(paginate);

/**
 * @typedef News
 */
const News = mongoose.model('News', newsSchema);

module.exports = News;
