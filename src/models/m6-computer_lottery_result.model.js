const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const computerLotteryResultSchema = mongoose.Schema(
  {
    type1: {
      type: String,
    },
    result1: {
      type: String,
    },
    type2: {
      type: String,
    },
    result2: {
      type: String,
    },
    type3: {
      type: String,
    },
    result3: {
      type: String,
    },
    dateStart: {
      type: Date,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
computerLotteryResultSchema.plugin(toJSON);
computerLotteryResultSchema.plugin(paginate);

/**
 * @typedef Province
 */
const ComputerLotteryResult = mongoose.model('ComputerLotteryResult', computerLotteryResultSchema);

module.exports = ComputerLotteryResult;
