const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const Power655Schema = mongoose.Schema(
  {
    dateStart: {
      type: Date,
    },
    setOfNumberWinningNumber: {
      type: String,
    },
    jackpot1: {
      type: String,
    },
    jackpot1AmountMatch: {
      type: String,
    },
    jackpot1NumberOfPrizes: {
      type: String,
    },
    jackpot2: {
      type: String,
    },
    jackpot2AmountMatch: {
      type: String,
    },
    jackpot2NumberOfPrizes: {
      type: String,
    },
    firstPrize: {
      type: String,
    },
    firstPrizeAmountMatch: {
      type: String,
    },
    firstPrizeNumberOfPrizes: {
      type: String,
    },
    secondPrize: {
      type: String,
    },
    secondPrizeAmountMatch: {
      type: String,
    },
    secondPrizeNumberOfPrizes: {
      type: String,
    },
    thirdPrize: {
      type: String,
    },
    thirdPrizeAmountMatch: {
      type: String,
    },
    thirdPrizeNumberOfPrizes: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
Power655Schema.plugin(toJSON);
Power655Schema.plugin(paginate);

/**
 * @typedef Province
 */
const Power655 = mongoose.model('Power655', Power655Schema);

module.exports = Power655;
