module.exports.Token = require('./token.model');
// ThanhVV
module.exports.Menu1 = require('./m0-menu1.model');
module.exports.Menu2 = require('./m0-menu2.model');
module.exports.User = require('./m0-user.model');
module.exports.Menu = require('./m1-menu.model');
module.exports.Region = require('./m2-region.model');
module.exports.Province = require('./m3-province.model');
module.exports.PrizeOpenSchedule = require('./m4-prize_open_schedule.model');
module.exports.Lottery = require('./m5-lottery.model');
module.exports.ComputerLotteryResult = require('./m6-computer_lottery_result.model');
module.exports.Power655 = require('./m7-power655.model');
module.exports.Power645 = require('./m8-power645.model');
module.exports.DreamDecoding = require('./m10-dream_decoding.model');
module.exports.Max4D = require('./m9-max4d.model');

// Admin
module.exports.Menu = require('./20menu.models');
module.exports.Role = require('./30role.models');
module.exports.RoleDetail = require('./40role-detail.models');
module.exports.Banner = require('./50banner.model');
module.exports.News = require('./60news.model');

module.exports.SoiCau = require('./70soi-cau.model');
module.exports.Loai = require('./80loai.model');


// Test
module.exports.ModelForTest = require('./90model-for-test.model');
