const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const menu2Schema = mongoose.Schema(
  {
    name: {
      type: String,
    },
    slug: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
menu2Schema.plugin(toJSON);
menu2Schema.plugin(paginate);

/**
 * @typedef Province
 */
const Menu2 = mongoose.model('Menu2', menu2Schema);

module.exports = Menu2;
