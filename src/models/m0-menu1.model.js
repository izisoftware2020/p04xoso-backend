const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const menu1Schema = mongoose.Schema(
  {
    name: {
      type: String,
    },
    slug: {
      type: String,
    },
    menu2: {
      type: Schema.Types.ObjectId,
      ref: 'Menu2',
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
menu1Schema.plugin(toJSON);
menu1Schema.plugin(paginate);

/**
 * @typedef Province
 */
const Menu1 = mongoose.model('Menu1', menu1Schema);

module.exports = Menu1;
