const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const lotterySchema = mongoose.Schema(
  {
    dateStart: {
      type: Date,
    },
    specialPrize: {
      type: String,
    },
    firstPrize: {
      type: String,
    },
    secondPrize: {
      type: String,
    },
    thirdPrize: {
      type: String,
    },
    fourthPrize: {
      type: String,
    },
    fifthPrize: {
      type: String,
    },
    sixthPrize: {
      type: String,
    },
    seventhPrize: {
      type: String,
    },
    eighthPrize: {
      type: String,
    },
    specialPrizeTemp: {
      type: String,
    },
    countTemp: {
      type: Number,
    },
    seventhPrizeTemp: {
      type: String,
    },
    countTemp1: {
      type: Number,
    },

    // Start giai 6 1
    sixthPrizeOneTemp: {
      type: String,
    },

    countsixthPrizeOne: {
      type: Number,
    },
    // End  giai 6 1

    // Start giai 6 2
    sixthPrizeTwoTemp: {
      type: String,
    },

    countsixthPrizeTwo: {
      type: Number,
    },
    // End giai 6 2

    // Start giai 6 3
    sixthPrizeThreeTemp: {
      type: String,
    },

    countsixthPrizeThree: {
      type: Number,
    },
    // End giai 6 3

    // Start giai 7 1
    seventPrizeOneTemp: {
      type: String,
    },

    countSeventhPrizeOne: {
      type: Number,
    },
    // End giai 7 1

    // Start giai 7 2
    seventPrizeTwoTemp: {
      type: String,
    },

    countSeventhPrizeTwo: {
      type: Number,
    },
    // End giai 7 2

    // Start giai 7 3
    seventPrizeThreeTemp: {
      type: String,
    },

    countSeventhPrizeThree: {
      type: Number,
    },
    // End giai 7 3

    // Start giai 7 4
    seventPrizeFourTemp: {
      type: String,
    },

    countSeventhPrizeFour: {
      type: Number,
    },
    // End giai 7 4

    province: {
      type: Schema.Types.ObjectId,
      ref: 'Province',
    },
    region: {
      type: Schema.Types.ObjectId,
      ref: 'Region',
    },
    headAndTail: {
      type: Object,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
lotterySchema.plugin(toJSON);
lotterySchema.plugin(paginate);

/**
 * @typedef Province
 */
const Lottery = mongoose.model('Lottery', lotterySchema);

module.exports = Lottery;
